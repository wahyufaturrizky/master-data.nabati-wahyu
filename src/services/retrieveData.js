import { Request, RequestPublic } from './headerRequest';
/**
 * @author wahyu fatur rizki (+62 822 7458 6011)
 * @return { obj }
 */

export const postLogin = (data) => {
	const response = RequestPublic().post('/api/v1/login', data);
	return response;
};

export const getDataListMasterSpecRoll = (data) => {
	const response = Request().get(
		`/api/v1/material/specification-roll?material_group=${data.material_group}&search=${data.search}`
	);
	return response;
};

export const getDataListCharacteritic = (data) => {
	const response = Request().get(
		`/api/v1/master/characteristic?material_group_id=${data.material_group_id}`
	);
	return response;
};

export const getDataListMasterSpecCartoonBox = (data) => {
	const response = Request().get(
		`/api/v1/material/specification-carton-box?material_group=${data.material_group}&search=${data.search}`
	);
	return response;
};

export const getDataListMasterCartoonBoxFluteGeneral = (data) => {
	const response = Request().get(
		`/api/v1/material/flute-general?material_group=${data.material_group}&search=${data.search}`
	);
	return response;
};

export const getDataListMasterCartoonBoxFluteVendor = (data) => {
	const response = Request().get(
		`/api/v1/material/flute-vendor?material_group=${data.material_group}&search=${data.search}`
	);
	return response;
};

export const getDataListMasterCartoonBoxLayerSekat = (data) => {
	const response = Request().get(
		`/api/v1/material/layer-sekat?material_group=${data.material_group}&search=${data.search}`
	);
	return response;
};

export const getDataListMasterSpec = () => {
	const response = Request().get('/api/v1/master/specification');
	return response;
};

export const getDataListVendor = () => {
	const response = Request().get('/api/v1/master/vendor');
	return response;
};

export const getDataListFluteVendorItem = (data) => {
	const response = Request().get(
		`/api/v1/material/flute-vendor-item?flute_vendor=${data.flute_vendor}&search=${data.search}`
	);
	return response;
};

export const getDataListLayerSekatItem = (data) => {
	const response = Request().get(
		`/api/v1/material/layer-sekat-item?layer_sekat_id=${data.layer_sekat_id}&search=${data.keyword}`
	);
	return response;
};

export const postDataListMasterSpec = (data) => {
	const response = Request().post('/api/v1/master/specification', data);
	return response;
};

export const postDataListMasterSpecRoll = (data) => {
	const response = Request().post('/api/v1/material/specification-roll', data);
	return response;
};

export const postDataListMasterSpecCartonBox = (data) => {
	const response = Request().post(
		'/api/v1/material/specification-carton-box',
		data
	);
	return response;
};

export const postDataListMasterCartoonBoxFluteGeneral = (data) => {
	const response = Request().post('/api/v1/material/flute-general', data);
	return response;
};

export const postDataListMasterFluteByVendor = (data) => {
	const response = Request().post('/api/v1/material/flute-vendor', data);
	return response;
};

export const postDataListMasterLayerSekat = (data) => {
	const response = Request().post('/api/v1/material/layer-sekat', data);
	return response;
};

export const postDataListFluteVendorItem = (data) => {
	const response = Request().post('/api/v1/material/flute-vendor-item', data);
	return response;
};

export const postDataListLayerSekatItem = (data) => {
	const response = Request().post('/api/v1/material/layer-sekat-item', data);
	return response;
};

export const deleteDataListMasterSpec = (id) => {
	const response = Request().delete(`/api/v1/master/specification/${id}`);
	return response;
};

export const deleteDataListMasterSpecRoll = (id) => {
	const response = Request().delete(
		`/api/v1/material/specification-roll/${id}`
	);
	return response;
};

export const deleteDataListMasterSpecCartonBox = (id) => {
	const response = Request().delete(
		`/api/v1/material/specification-carton-box/${id}`
	);
	return response;
};

export const deleteDataListMasterCartoonBoxFluteGeneral = (id) => {
	const response = Request().delete(`/api/v1/material/flute-general/${id}`);
	return response;
};

export const deleteDataListMasterCartoonBoxFluteByVendor = (id) => {
	const response = Request().delete(`/api/v1/material/flute-vendor/${id}`);
	return response;
};

export const deleteDataListMasterCartoonBoxLayerSekat = (id) => {
	const response = Request().delete(`/api/v1/material/layer-sekat/${id}`);
	return response;
};

export const deleteDataListFluteVendorItem = (id) => {
	const response = Request().delete(`/api/v1/material/flute-vendor-item/${id}`);
	return response;
};

export const deleteDataListLayerSekatItem = (id) => {
	const response = Request().delete(`/api/v1/material/layer-sekat-item/${id}`);
	return response;
};

export const putDataListMasterSpec = (id, data) => {
	const response = Request().put(`/api/v1/master/specification/${id}`, data);
	return response;
};

export const putDataListMasterSpecRoll = (id, data) => {
	const response = Request().put(
		`/api/v1/material/specification-roll/${id}`,
		data
	);
	return response;
};

export const putDataListMasterSpecCartonBox = (id, data) => {
	const response = Request().put(
		`/api/v1/material/specification-carton-box/${id}`,
		data
	);
	return response;
};

export const putDataListMasterCartoonBoxFluteGeneral = (id, data) => {
	const response = Request().put(`/api/v1/material/flute-general/${id}`, data);
	return response;
};

export const putDataListMasterCartoonBoxFluteByVendor = (id, data) => {
	const response = Request().put(`/api/v1/material/flute-vendor/${id}`, data);
	return response;
};

export const putDataListMasterCartoonBoxLayerSekat = (id, data) => {
	const response = Request().put(`/api/v1/material/layer-sekat/${id}`, data);
	return response;
};

export const putDataListFluteVendorItem = (id, data) => {
	const response = Request().put(
		`/api/v1/material/flute-vendor-item/${id}`,
		data
	);
	return response;
};

export const putDataListLayerSekatItem = (id, data) => {
	const response = Request().put(
		`/api/v1/material/layer-sekat-item/${id}`,
		data
	);
	return response;
};
