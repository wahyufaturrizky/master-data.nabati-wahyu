/* eslint-disable react-hooks/rules-of-hooks */
import {
	CaretDownOutlined,
	DatabaseOutlined,
	MenuOutlined,
	UserOutlined,
} from '@ant-design/icons';
import {
	Avatar,
	Dropdown,
	Image,
	Layout,
	Menu,
	Space,
	Typography,
	Modal,
} from 'antd';
import React, { useState } from 'react';
import { ColorPrimaryEnum } from '../styles/Colors';
import '../styles/Dashboard.css';

const { Title } = Typography;
const { Header, Footer, Sider } = Layout;
const { SubMenu } = Menu;

const rootSubmenuVendorKeys = [
	'subvendor1',
	'subvendor2',
	'subvendor3',
	'subvendor4',
];

const menuProfile = (props, modal) => {
	return (
		<Menu>
			<Menu.Item
				onClick={() => {
					modal.confirm({
						title: 'Message system!',
						content: <p>Apakah anda ingin keluar dari halaman web ini?</p>,
						onOk: () => {
							localStorage.removeItem('dataUser');
							localStorage.removeItem('dataUser');
							props.history.push('/');
						},
						onCancel: () => {},
					});
				}}>
				Log out
			</Menu.Item>
		</Menu>
	);
};

const WrapperContent = (props) => {
	const [modal, contextHolder] = Modal.useModal();
	const [openKeys, setOpenKeys] = useState(
		props?.match?.url === '/master-specification' ||
			props?.match?.url === '/master-specification-roll' ||
			props?.match?.url === '/master-specification-cartoon-box' ||
			props?.match?.url ===
				'/master-specification-cartoon-box-flute-vendor-item' ||
			props?.match?.url === '/master-specification-cartoon-box-layer-sekat-item'
			? ['subvendor1']
			: props?.match?.url === '/master-design'
			? ['subvendor1']
			: props?.match?.url === '/rangking-weighting'
			? ['subvendor1']
			: props?.match?.url === '/commit-utama'
			? ['subvendor1']
			: props?.match?.url === '/commit-reguler'
			? ['subvendor1']
			: props?.match?.url === '/standard-allocation'
			? ['subvendor1']
			: props?.match?.url === '/upload-rfq'
			? ['subvendor2']
			: props?.match?.url === '/approval-rfq'
			? ['subvendor2']
			: props?.match?.url === '/vendor-performance'
			? ['subvendor2']
			: props?.match?.url === '/upload-budget'
			? ['subvendor2']
			: props?.match?.url === '/cockpit-vendor-alloc'
			? ['subvendor2']
			: props?.match?.url === '/submit-vendor-alloc'
			? ['subvendor2']
			: props?.match?.url === '/approval-vendor-alloc'
			? ['subvendor2']
			: props?.match?.url === '/rfq-report'
			? ['subvendor3']
			: props?.match?.url === '/report-vendor-performance'
			? ['subvendor3']
			: props?.match?.url === '/price-and-ranking'
			? ['subvendor3']
			: props?.match?.url === '/mrp-report'
			? ['subvendor3']
			: props?.match?.url === '/pr-report'
			? ['subvendor3']
			: props?.match?.url === '/va-suggestion-report'
			? ['subvendor3']
			: props?.match?.url === '/va-adjustment'
			? ['subvendor3']
			: props?.match?.url === '/budget-report'
			? ['subvendor3']
			: props?.match?.url === '/simulation-allocation'
			? ['subvendor3']
			: props?.match?.url === '/final-allocation'
			? ['subvendor3']
			: null
	);
	const [collapsed, setCollapsed] = useState(false);
	const listMenu = [
		{
			title: 'Master Data',
			nameIcon: 'DatabaseOutlined',
			subMenu: [
				{
					title: 'Master Specification',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/master-specification'),
				},
				{
					title: 'MD Design',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/master-design'),
				},
				{
					title: 'Rangking Weighting',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/rangking-weighting'),
				},
				{
					title: 'MD Commit Utama',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/commit-utama'),
				},
				{
					title: 'MD Commit Reguler',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/commit-reguler'),
				},
				{
					title: 'Standard Allocation',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/standard-allocation'),
				},
			],
		},
		{
			title: 'Transaction',
			nameIcon: 'DatabaseOutlined',
			subMenu: [
				{
					title: 'Upload RFQ',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/upload-rfq'),
				},
				{
					title: 'Approval RFQ',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/approval-rfq'),
				},
				{
					title: 'Vendor Performance',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/vendor-performance'),
				},
				{
					title: 'Upload Budget',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/upload-budget'),
				},
				{
					title: 'Cockpit Vendor Alloc',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/cockpit-vendor-alloc'),
				},
				{
					title: 'Submit Vendor Alloc',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/submit-vendor-alloc'),
				},
				{
					title: 'Approval Vendor Alloc',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/approval-vendor-alloc'),
				},
			],
		},
		{
			title: 'Report',
			nameIcon: 'DatabaseOutlined',
			subMenu: [
				{
					title: 'RFQ Report',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/rfq-report'),
				},
				{
					title: 'Vendor Performance Report',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/report-vendor-performance'),
				},
				{
					title: 'Price & Ranking',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/price-and-ranking'),
				},
				{
					title: 'MRP Report',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/mrp-report'),
				},
				{
					title: 'PR Report',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/pr-report'),
				},
				{
					title: 'VA Suggestion',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/va-suggestion-report'),
				},
				{
					title: 'VA Adjustment',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/va-adjustment'),
				},
				{
					title: 'Budget Report',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/budget-report'),
				},
				{
					title: 'Simulation Allocation',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/simulation-allocation'),
				},
				{
					title: 'Final Allocation',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/final-allocation'),
				},
			],
		},
		{
			title: 'Config',
			nameIcon: 'DatabaseOutlined',
			subMenu: [
				{
					title: 'User',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/user'),
				},
				{
					title: 'Company',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/company'),
				},
				{
					title: 'Plant',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/plant'),
				},
				{
					title: 'Vendor',
					nameIcon: 'DatabaseOutlined',
					onClick: () => props.history.push('/vendor'),
				},
			],
		},
	];

	const onOpenChange = (keys) => {
		const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
		if (rootSubmenuVendorKeys.indexOf(latestOpenKey) === -1) {
			setOpenKeys(keys);
		} else {
			setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
		}
	};

	return (
		<Layout>
			<Sider
				width={250}
				style={{
					backgroundColor: ColorPrimaryEnum.greenTosca,
				}}
				trigger={null}
				collapsible
				collapsed={collapsed}>
				<div className='logo'>
					<Image
						width={394 * (collapsed ? 0.2 : 0.4)}
						height={217.863 * (collapsed ? 0.2 : 0.4)}
						style={{ alignSelf: 'center' }}
						src='https://i.ibb.co/7ztPG9q/LOGO-POWERED-BY-e-DOT-e-PROC.png'
						fallback='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=='
					/>
				</div>
				<Menu
					onOpenChange={onOpenChange}
					style={{ backgroundColor: ColorPrimaryEnum.greenTosca }}
					mode='inline'
					openKeys={openKeys}
					selectedKeys={
						props?.match?.url === '/master-specification' ||
						props?.match?.url === '/master-specification-roll' ||
						props?.match?.url === '/master-specification-cartoon-box' ||
						props?.match?.url ===
							'/master-specification-cartoon-box-flute-vendor-item' ||
						props?.match?.url ===
							'/master-specification-cartoon-box-layer-sekat-item'
							? ['submaster1']
							: props?.match?.url === '/master-design'
							? ['submaster2']
							: props?.match?.url === '/rangking-weighting'
							? ['submaster3']
							: props?.match?.url === '/commit-utama'
							? ['submaster4']
							: props?.match?.url === '/commit-reguler'
							? ['submaster5']
							: props?.match?.url === '/standard-allocation'
							? ['submaster6']
							: props?.match?.url === '/upload-rfq'
							? ['subtransaction1']
							: props?.match?.url === '/approval-rfq'
							? ['subtransaction2']
							: props?.match?.url === '/vendor-performance'
							? ['subtransaction3']
							: props?.match?.url === '/upload-budget'
							? ['subtransaction4']
							: props?.match?.url === '/cockpit-vendor-alloc'
							? ['subtransaction5']
							: props?.match?.url === '/submit-vendor-alloc'
							? ['subtransaction6']
							: props?.match?.url === '/approval-vendor-alloc'
							? ['subtransaction7']
							: props?.match?.url === '/rfq-report'
							? ['subreport1']
							: props?.match?.url === '/report-vendor-performance'
							? ['subreport2']
							: props?.match?.url === '/price-and-ranking'
							? ['subreport3']
							: props?.match?.url === '/mrp-report'
							? ['subreport4']
							: props?.match?.url === '/pr-report'
							? ['subreport5']
							: props?.match?.url === '/va-suggestion-report'
							? ['subreport6']
							: props?.match?.url === '/va-adjustment'
							? ['subreport7']
							: props?.match?.url === '/budget-report'
							? ['subreport8']
							: props?.match?.url === '/simulation-allocation'
							? ['subreport9']
							: props?.match?.url === '/final-allocation'
							? ['subreport10']
							: props?.match?.url === '/user'
							? ['subconfig1']
							: props?.match?.url === '/company'
							? ['subconfig2']
							: props?.match?.url === '/plant'
							? ['subconfig3']
							: props?.match?.url === '/vendor'
							? ['subconfig4']
							: null
					}>
					<Menu.ItemGroup key='g1' title='Vendor Allocation'>
						{listMenu?.map((subDataVendor, subIndexVendor) => {
							const { title, nameIcon, subMenu } = subDataVendor;
							return (
								<SubMenu
									className='submenu'
									key={`subvendor${subIndexVendor + 1}`}
									icon={
										nameIcon === 'DatabaseOutlined' ? (
											<DatabaseOutlined />
										) : null
									}
									title={title}>
									{subMenu &&
										subMenu?.map((subData, subIndex) => {
											const { title, nameIcon, onClick } = subData;
											return (
												<Menu.Item
													onClick={onClick}
													key={
														title === 'Master Specification'
															? `submaster${subIndex + 1}`
															: title === 'MD Design'
															? `submaster${subIndex + 1}`
															: title === 'Rangking Weighting'
															? `submaster${subIndex + 1}`
															: title === 'MD Commit Utama'
															? `submaster${subIndex + 1}`
															: title === 'MD Commit Reguler'
															? `submaster${subIndex + 1}`
															: title === 'Standard Allocation'
															? `submaster${subIndex + 1}`
															: title === 'Upload RFQ'
															? `subtransaction${subIndex + 1}`
															: title === 'Approval RFQ'
															? `subtransaction${subIndex + 1}`
															: title === 'Vendor Performance'
															? `subtransaction${subIndex + 1}`
															: title === 'Upload Budget'
															? `subtransaction${subIndex + 1}`
															: title === 'Cockpit Vendor Alloc'
															? `subtransaction${subIndex + 1}`
															: title === 'Submit Vendor Alloc'
															? `subtransaction${subIndex + 1}`
															: title === 'Approval Vendor Alloc'
															? `subtransaction${subIndex + 1}`
															: title === 'RFQ Report'
															? `subreport${subIndex + 1}`
															: title === 'Vendor Performance Report'
															? `subreport${subIndex + 1}`
															: title === 'Price & Ranking'
															? `subreport${subIndex + 1}`
															: title === 'MRP Report'
															? `subreport${subIndex + 1}`
															: title === 'PR Report'
															? `subreport${subIndex + 1}`
															: title === 'VA Suggestion'
															? `subreport${subIndex + 1}`
															: title === 'VA Adjustment'
															? `subreport${subIndex + 1}`
															: title === 'Budget Report'
															? `subreport${subIndex + 1}`
															: title === 'Simulation Allocation'
															? `subreport${subIndex + 1}`
															: title === 'Final Allocation'
															? `subreport${subIndex + 1}`
															: title === 'User'
															? `subconfig${subIndex + 1}`
															: title === 'Company'
															? `subconfig${subIndex + 1}`
															: title === 'Plant'
															? `subconfig${subIndex + 1}`
															: title === 'Vendor'
															? `subconfig${subIndex + 1}`
															: null
													}
													icon={
														nameIcon === 'DatabaseOutlined' ? (
															<DatabaseOutlined />
														) : null
													}>
													{title}
												</Menu.Item>
											);
										})}
								</SubMenu>
							);
						})}
					</Menu.ItemGroup>
				</Menu>
			</Sider>
			<Layout className='site-layout'>
				<Header
					className='site-layout-background'
					style={{ padding: '0px 16px' }}>
					<div
						style={{
							justifyContent: 'space-between',
							flexDirection: 'row',
							display: 'flex',
							alignItems: 'center',
						}}>
						<MenuOutlined
							onClick={() => setCollapsed(!collapsed)}
							className='trigger'
						/>
						<Title level={5}>E - Procurement</Title>
						<div>
							<Space>
								<Avatar size='default' icon={<UserOutlined />} />
								<Dropdown overlay={menuProfile(props, modal)}>
									<span>
										Wahyu <CaretDownOutlined />
									</span>
								</Dropdown>
							</Space>
						</div>
					</div>
				</Header>
				{props.children}
				<Footer style={{ textAlign: 'center' }}>
					<p>
						Master Data EPROC Vendor Allocation @2021 Created by{' '}
						<span>
							<a
								href='https://www.nabatisnack.co.id/'
								target='_blank'
								rel='noreferrer'>
								Team Dev Kaldu Sari Nabati
							</a>
						</span>
					</p>
				</Footer>
			</Layout>
			{contextHolder}
		</Layout>
	);
};

export default WrapperContent;
