import { DownloadOutlined, EyeOutlined } from '@ant-design/icons';
import {
	Button,
	DatePicker,
	Layout,
	Radio,
	Select,
	Typography,
	Space,
	Row,
	Table,
} from 'antd';
import React, { useState } from 'react';
import { ColorBaseEnum, ColorPrimaryEnum } from '../styles/Colors';
import { MarginEnum } from '../styles/Spacer';

const { Content } = Layout;
const { Option } = Select;
const { Title, Text } = Typography;

const RFQReport = (props) => {
	const [isLoading, setIsLoading] = useState(false);

	const dataTable = [
		{
			key: 1,
			vendor: '200012',
			vendorDesc: 'Century MMS PT',
			layerOne: 'PET12',
			layerTwo: 'KZMB',
			layerThree: 'CPP25',
			colorMin: '1',
			colorMax: '3',
			allPlant: 'Yes',
			plant: 'N/A',
			validFrom: '01-04-2021',
			validTo: '01-08-2021',
			price: '1.200',
			currency: 'N/A',
			per: '1',
			unit: '1',
			pirCategory: '01-04-2021',
			vatCode: '01-04-2021',
			purchOrg: 'N/A',
			baseUom: 'N/A',
			status: 'Approve',
		},
	];

	const columnsTable = [
		{
			title: 'No',
			dataIndex: 'key',
			key: 'key',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Vendor',
			dataIndex: 'vendor',
			key: 'vendor',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Vendor Description',
			dataIndex: 'vendorDesc',
			key: 'vendorDesc',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Layer 1',
			dataIndex: 'layerOne',
			key: 'layerOne',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Layer 2',
			dataIndex: 'layerTwo',
			key: 'layerTwo',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Layer 3',
			dataIndex: 'layerThree',
			key: 'layerThree',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Color Min',
			dataIndex: 'colorMin',
			key: 'colorMin',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Color Max',
			dataIndex: 'colorMax',
			key: 'colorMax',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'All Plant',
			dataIndex: 'allPlant',
			key: 'allPlant',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Plant',
			dataIndex: 'plant',
			key: 'plant',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Valid From',
			dataIndex: 'validFrom',
			key: 'validFrom',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Valid To',
			dataIndex: 'validTo',
			key: 'validTo',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Price',
			dataIndex: 'price',
			key: 'price',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Currency',
			dataIndex: 'currency',
			key: 'currency',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Per',
			dataIndex: 'per',
			key: 'per',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Unit',
			dataIndex: 'unit',
			key: 'unit',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'PIR Category',
			dataIndex: 'pirCategory',
			key: 'pirCategory',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'VAT Code',
			dataIndex: 'vatCode',
			key: 'vatCode',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Purch Org',
			dataIndex: 'purchOrg',
			key: 'purchOrg',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Base Uom',
			dataIndex: 'baseUom',
			key: 'baseUom',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Status',
			dataIndex: 'status',
			key: 'status',
			render: (text) => <p>{text}</p>,
		},
	];

	return (
		<Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
			<Title level={4}>RFQ Report</Title>
			<div className='site-layout-background' style={{ padding: 24 }}>
				<div style={{ marginBottom: MarginEnum['2x'] }}>
					<Radio.Group value='sku'>
						<Radio value='sku'>SKU</Radio>
						<Radio value='specification'>Specification</Radio>
					</Radio.Group>
				</div>

				<Row
					style={{
						marginBottom: MarginEnum['2x'],
					}}>
					<Space>
						<Space direction='vertical'>
							<Text>Material Group</Text>
							<Select
								allowClear
								showSearch
								style={{ width: 200 }}
								placeholder='Select'
								optionFilterProp='children'
								onChange={(e) => console.log(`onChange company ${e}`)}
								onFocus={(e) => console.log(`onFocus company ${e}`)}
								onBlur={(e) => console.log(`onBlur company ${e}`)}
								onSearch={(e) => console.log(`onSearch company ${e}`)}
								filterOption={(input, option) =>
									option.children.toLowerCase().indexOf(input.toLowerCase()) >=
									0
								}>
								<Option value='jack'>Jack</Option>
								<Option value='lucy'>Lucy</Option>
								<Option value='tom'>Tom</Option>
							</Select>
						</Space>
						<Space direction='vertical'>
							<Text>Vendor</Text>
							<Select
								allowClear
								showSearch
								style={{ width: 200 }}
								placeholder='Select'
								optionFilterProp='children'
								onChange={(e) => console.log(`onChange vendor ${e}`)}
								onFocus={(e) => console.log(`onFocus vendor ${e}`)}
								onBlur={(e) => console.log(`onBlur vendor ${e}`)}
								onSearch={(e) => console.log(`onSearch vendor ${e}`)}
								filterOption={(input, option) =>
									option.children.toLowerCase().indexOf(input.toLowerCase()) >=
									0
								}>
								<Option value='jack'>Jack</Option>
								<Option value='lucy'>Lucy</Option>
								<Option value='tom'>Tom</Option>
							</Select>
						</Space>
						<Space direction='vertical'>
							<Text>Valid From</Text>
							<DatePicker />
						</Space>
						<Space direction='vertical'>
							<Text>Material No</Text>
							<Select
								allowClear
								showSearch
								style={{ width: 200 }}
								placeholder='Select'
								optionFilterProp='children'
								onChange={(e) => console.log(`onChange materialNo ${e}`)}
								onFocus={(e) => console.log(`onFocus materialNo ${e}`)}
								onBlur={(e) => console.log(`onBlur materialNo ${e}`)}
								onSearch={(e) => console.log(`onSearch materialNo ${e}`)}
								filterOption={(input, option) =>
									option.children.toLowerCase().indexOf(input.toLowerCase()) >=
									0
								}>
								<Option value='jack'>Jack</Option>
								<Option value='lucy'>Lucy</Option>
								<Option value='tom'>Tom</Option>
							</Select>
						</Space>
						<Space direction='vertical'>
							<Text>Status</Text>
							<Select
								allowClear
								showSearch
								style={{ width: 200 }}
								placeholder='Select'
								optionFilterProp='children'
								onChange={(e) => console.log(`onChange status ${e}`)}
								onFocus={(e) => console.log(`onFocus status ${e}`)}
								onBlur={(e) => console.log(`onBlur status ${e}`)}
								onSearch={(e) => console.log(`onSearch status ${e}`)}
								filterOption={(input, option) =>
									option.children.toLowerCase().indexOf(input.toLowerCase()) >=
									0
								}>
								<Option value='jack'>Jack</Option>
								<Option value='lucy'>Lucy</Option>
								<Option value='tom'>Tom</Option>
							</Select>
						</Space>
						<Space direction='vertical'>
							<Text style={{ color: ColorBaseEnum.white }}>text</Text>
							<Button
								style={{
									backgroundColor: ColorPrimaryEnum.redVelvet,
									border: `1px solid ${ColorPrimaryEnum.redVelvet}`,
								}}
								type='primary'
								icon={<EyeOutlined />}>
								View
							</Button>
						</Space>
						<Space direction='vertical'>
							<Text style={{ color: ColorBaseEnum.white }}>text</Text>
							<Button
								style={{
									backgroundColor: ColorPrimaryEnum.greenTosca,
									border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
								}}
								type='primary'
								icon={<DownloadOutlined />}>
								Download
							</Button>
						</Space>
					</Space>
				</Row>
				<Table
					loading={isLoading}
					bordered
					columns={columnsTable}
					dataSource={dataTable}
					scroll={{ x: 1800 }}
				/>
			</div>
		</Content>
	);
};

export default RFQReport;
