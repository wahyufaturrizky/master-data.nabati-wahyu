/* eslint-disable react-hooks/exhaustive-deps */
import {
	ArrowLeftOutlined,
	DeleteOutlined,
	EditOutlined,
} from '@ant-design/icons';
import {
	Button,
	Col,
	Form,
	Input,
	Layout,
	Modal,
	Row,
	Select,
	Space,
	Table,
	Tabs,
	Typography,
} from 'antd';
import moment from 'moment';
import React, { useCallback, useEffect, useState } from 'react';
import {
	deleteDataListLayerSekatItem,
	getDataListLayerSekatItem,
	postDataListLayerSekatItem,
	postLogin,
	putDataListLayerSekatItem,
} from '../services/retrieveData';

const { Content } = Layout;
const { Option } = Select;
const { Title } = Typography;
const { TabPane } = Tabs;

const formItemLayout = {
	labelCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 8,
		},
	},
	wrapperCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 16,
		},
	},
};

const MasterSpesificationLayerSekatItem = (props) => {
	const [modal, contextHolder] = Modal.useModal();
	const [form] = Form.useForm();
	const [dataListFluteVendorItem, setDataListFluteVendorItem] = useState([]);
	const [isModalActionVisible, setIsModalActionVisible] = useState({
		dataRow: null,
		typeAction: '',
		isShowModalAction: false,
	});
	const [isLoading, setIsLoading] = useState(false);
	const { isShowModalAction, typeAction, dataRow } = isModalActionVisible;

	const fetchDataListFluteVendorItem = useCallback(async ({ keyword }) => {
		setIsLoading(true);
		const data = {
			layer_sekat_id: props.location.state.id,
			keyword: keyword || '',
		};

		const res = await getDataListLayerSekatItem(data).catch((err) => {
			setIsLoading(false);
			if (err?.response?.data?.message === 'Unauthenticated.') {
				modal.warning({
					title: 'Warning Message!',
					content: (
						<p>Your session has expired, click OK to renew your session</p>
					),
					onOk: async () => {
						setIsLoading(true);
						const dataLogin = await localStorage.getItem('dataUser');
						const dataLoginParse = JSON.parse(dataLogin);
						let data = new FormData();

						data.append('username', dataLoginParse.username || 'user_demo');
						data.append('password', dataLoginParse.password || '12345');
						const res = await postLogin(data).catch((err) => {
							window.alert('Failed login');
							setIsLoading(false);
						});

						if (res && res.status === 200) {
							if (res.data === '') {
								setIsLoading(false);
								modal.error({
									title: 'Failed login!',
									content: (
										<p>Username/password is not correct, please try again</p>
									),
									onOk: () => {},
								});
							} else {
								localStorage.setItem('dataUser', JSON.stringify(res.data));
								fetchDataListFluteVendorItem({});
								setIsLoading(false);
							}
						}
					},
				});
			} else {
				modal.error({
					title: 'Failed Message!',
					content: (
						<p>{err?.response?.data?.message || 'Internal server error'}</p>
					),
					onOk: () => {},
				});
			}
		});

		if (res && res.status === 200) {
			setIsLoading(false);
			setDataListFluteVendorItem(res.data.items.data);
		}
	}, []);

	useEffect(() => {
		fetchDataListFluteVendorItem({});
	}, [fetchDataListFluteVendorItem]);

	const onFinish = async (values) => {
		setIsLoading(true);
		let data = {
			value: values.value,
			layer_sekat_id: props.location.state.id,
			currency: values.currency,
		};

		const res = await postDataListLayerSekatItem(data).catch((err) => {
			modal.error({
				title: 'Failed Message!',
				content: <p>{err.response.data.message || 'Internal server error'}</p>,
				onOk: () => {
					form.resetFields();
				},
				onCancel: () => {
					form.resetFields();
				},
			});
			setIsLoading(false);
		});

		if (res && res.status === 200) {
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					form.resetFields();
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
					fetchDataListFluteVendorItem({});
				},
			});
			setIsLoading(false);
		}
	};

	const onEdit = async (values) => {
		setIsLoading(true);

		let data = {
			value: values.value,
			layer_sekat_id: props.location.state.id,
			currency: values.currency,
		};

		const res = await putDataListLayerSekatItem(dataRow.id, data).catch(
			(err) => {
				modal.error({
					title: 'Failed Message!',
					content: <p>{err.response.data.message}</p>,
				});
				setIsLoading(false);
			}
		);

		if (res && res.status === 200) {
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
					fetchDataListFluteVendorItem({});
				},
			});
		}
	};

	const onDelete = async (values) => {
		setIsLoading(true);
		const res = await deleteDataListLayerSekatItem(values).catch((err) => {
			modal.error({
				title: 'Failed Message!',
				content: <p>{err.response.data.message || 'Internal server error'}</p>,
				onOk: () => {
					form.resetFields();
				},
			});
			setIsLoading(false);
		});

		if (res && res.status === 200) {
			setIsLoading(false);
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					form.resetFields();
					fetchDataListFluteVendorItem({});
				},
			});
		}
	};

	let columnsTable = [
		{
			title: 'No',
			dataIndex: 'id',
			key: 'id',
			render: (text, rowData, index) => <p>{index + 1}</p>,
		},
		{
			title: 'Layer Sekat',
			dataIndex: 'layer_sekat_id',
			key: 'layer_sekat_id',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Characteristic',
			dataIndex: 'characteristic',
			key: 'characteristic',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Characteristic Des',
			dataIndex: 'characteristic_description',
			key: 'characteristic_description',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Spec ID',
			dataIndex: 'material_specification_id',
			key: 'material_specification_id',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Carton Type',
			dataIndex: 'carton_type',
			key: 'carton_type',
			render: (text) => <p>{text || 'N/A'}</p>,
		},
		{
			title: 'Value',
			dataIndex: 'value',
			key: 'value',
			render: (text) => <p>{text || 'N/A'}</p>,
		},
		{
			title: 'Currency',
			dataIndex: 'currency',
			key: 'currency',
			render: (text) => <p>{text || 'N/A'}</p>,
		},
		{
			title: 'Constanta',
			dataIndex: 'constanta',
			key: 'constanta',
			render: (text) => <p>{text || 'N/A'}</p>,
		},
		{
			title: 'Type',
			dataIndex: 'type',
			key: 'type',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Created',
			dataIndex: 'created_at',
			key: 'created_at',
			render: (text) => <p>{moment(text).format('YYYY-MM-DD')}</p>,
		},
		{
			title: 'Updated',
			dataIndex: 'updated_at',
			key: 'updated_at',
			render: (text) => <p>{moment(text).format('YYYY-MM-DD')}</p>,
		},
		{
			title: 'UUID',
			dataIndex: 'uuid',
			key: 'uuid',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Action',
			key: 'action',
			render: (text, record) => {
				return (
					<Space size='middle'>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'EDIT',
								})
							}
							type='default'
							icon={<EditOutlined />}>
							Edit
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									...isModalActionVisible,
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'DELETE',
								})
							}
							type='danger'
							icon={<DeleteOutlined />}>
							Delete
						</Button>
					</Space>
				);
			},
		},
	];

	const onSearch = (data) => {
		fetchDataListFluteVendorItem({ keyword: data });
	};

	return (
		<Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
			<Title level={4}>Master Specification</Title>
			<div className='site-layout-background' style={{ padding: 24 }}>
				<Row justify='space-between'>
					<Title level={4}>
						{props.location.state.vendor_code} -{' '}
						{props.location.state.vendor_name}
					</Title>
					<Button
						onClick={() => props.history.goBack()}
						type='default'
						icon={<ArrowLeftOutlined />}>
						Back to list
					</Button>
				</Row>
				<Tabs defaultActiveKey='1'>
					<TabPane tab='Specification' key='1'>
						<Row
							justify='space-between'
							style={{
								marginBottom: 12,
							}}>
							<Input.Search
								onSearch={onSearch}
								loading={isLoading}
								allowClear
								style={{ width: '40%' }}
							/>
						</Row>
						<Table
							loading={isLoading}
							bordered
							rowClassName={(record, index) => {
								if (index % 2 === 1) {
									return 'color-gray-2';
								} else {
									return 'color-gray-1';
								}
							}}
							columns={columnsTable.filter(
								(col) =>
									col.dataIndex !== 'uuid' &&
									col.dataIndex !== 'layer_sekat_id' &&
									col.dataIndex !== 'material_specification_id'
							)}
							dataSource={dataListFluteVendorItem}
						/>
					</TabPane>
				</Tabs>
			</div>

			<Modal
				title={typeAction}
				forceRender={false}
				visible={isShowModalAction}
				width={typeAction === 'ADD' || typeAction === 'EDIT' ? 1000 : undefined}
				afterClose={() => form.resetFields()}
				onOk={() => {
					if (typeAction === 'DELETE') {
						if (isLoading) {
							return;
						} else {
							onDelete(dataRow.id);
						}
					} else {
						form.resetFields();
						setIsModalActionVisible({
							...isModalActionVisible,
							isShowModalAction: false,
						});
					}
				}}
				onCancel={() => {
					form.resetFields();
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
				}}>
				{typeAction === 'VIEW' ? (
					dataRow &&
					Object.keys(dataRow).map((data, index) => {
						if (data === 'key' || data === 'guid') {
							return null;
						}
						return (
							<p key={index} style={{ fontWeight: 'bold', color: '#595959' }}>
								{data.replace('_', ' ')} :{' '}
								<span style={{ fontWeight: 'normal' }}>{dataRow[data]}</span>
							</p>
						);
					})
				) : typeAction === 'EDIT' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='edit'
						onFinish={onEdit}
						fields={[
							{
								name: ['value'],
								value: dataRow.value,
							},
							{
								name: ['currency'],
								value: dataRow.currency,
							},
						]}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='value'
									label='Value'
									rules={[
										{
											required: true,
											message: 'Please input your value!',
										},
									]}
									hasFeedback>
									<Input type='number' />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='currency'
									label='Currency'
									rules={[
										{
											required: true,
											message: 'Please input your currency!',
										},
									]}
									hasFeedback>
									<Select placeholder='select your currency'>
										<Option key={1} value={'IDR'}>
											{'IDR'}
										</Option>
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<Form.Item hidden name='guid' label='guid'>
							<Input />
						</Form.Item>
						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Save
								</Button>
							</Form.Item>
						</div>
					</Form>
				) : typeAction === 'ADD' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='add'
						onFinish={onFinish}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='value'
									label='Value'
									rules={[
										{
											required: true,
											message: 'Please input your value!',
										},
									]}
									hasFeedback>
									<Input type='number' />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='currency'
									label='Currency'
									rules={[
										{
											required: true,
											message: 'Please input your currency!',
										},
									]}
									hasFeedback>
									<Select placeholder='select your currency'>
										<Option key={1} value={'IRD'}>
											{'IRD'}
										</Option>
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Create
								</Button>
							</Form.Item>
						</div>
					</Form>
				) : (
					<p>
						Apakah anda ingin menghapus daata <b>{dataRow?.characteristic}</b>{' '}
						ini ?
					</p>
				)}
			</Modal>
			{contextHolder}
		</Content>
	);
};

export default MasterSpesificationLayerSekatItem;
