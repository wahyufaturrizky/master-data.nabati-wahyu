/* eslint-disable react-hooks/exhaustive-deps */
import {
	DeleteOutlined,
	EditOutlined,
	FileAddOutlined,
	UnorderedListOutlined,
} from '@ant-design/icons';
import {
	Button,
	Col,
	Form,
	Input,
	Layout,
	Modal,
	Row,
	Select,
	Space,
	Table,
	Typography,
} from 'antd';
import moment from 'moment';
import React, { useCallback, useEffect, useState } from 'react';
import {
	deleteDataListMasterSpec,
	getDataListMasterSpec,
	postDataListMasterSpec,
	postLogin,
	putDataListMasterSpec,
} from '../services/retrieveData';
import { ColorPrimaryEnum } from '../styles/Colors';

const { Content } = Layout;
const { Title } = Typography;
const { Option } = Select;

const formItemLayout = {
	labelCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 8,
		},
	},
	wrapperCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 16,
		},
	},
};

const MasterSpesification = (props) => {
	const [modal, contextHolder] = Modal.useModal();
	const [form] = Form.useForm();
	const [dataListMasterSpec, setDataListMasterSpec] = useState([]);
	const [dataPriority, setDataPriority] = useState(null);
	const [isModalActionVisible, setIsModalActionVisible] = useState({
		dataRow: null,
		typeAction: '',
		isShowModalAction: false,
	});
	const [isLoading, setIsLoading] = useState(false);
	const { isShowModalAction, typeAction, dataRow } = isModalActionVisible;

	const fetchDataListMasterSpec = useCallback(async () => {
		setIsLoading(true);
		const res = await getDataListMasterSpec().catch((err) => {
			setIsLoading(false);

			if (err?.response?.data?.message === 'Unauthenticated.') {
				modal.warning({
					title: 'Warning Message!',
					content: (
						<p>Your session has expired, click OK to renew your session</p>
					),
					onOk: async () => {
						setIsLoading(true);
						const dataLogin = await localStorage.getItem('dataUser');
						const dataLoginParse = JSON.parse(dataLogin);
						let data = new FormData();

						data.append('username', dataLoginParse.username || 'user_demo');
						data.append('password', dataLoginParse.password || '12345');
						const res = await postLogin(data).catch((err) => {
							window.alert('Failed login');
							setIsLoading(false);
						});

						if (res && res.status === 200) {
							if (res.data === '') {
								setIsLoading(false);
								modal.error({
									title: 'Failed login!',
									content: (
										<p>Username/password is not correct, please try again</p>
									),
									onOk: () => {},
								});
							} else {
								localStorage.setItem('dataUser', JSON.stringify(res.data));
								fetchDataListMasterSpec();
								setIsLoading(false);
							}
						}
					},
				});
			} else {
				modal.error({
					title: 'Failed Message!',
					content: (
						<p>{err?.response?.data?.message || 'Internal server error'}</p>
					),
					onOk: () => {},
				});
			}
		});

		if (res && res.status === 200) {
			setIsLoading(false);
			setDataListMasterSpec(res.data.items.data);
			setDataPriority(res.data.priority);
		}
	}, []);

	useEffect(() => {
		fetchDataListMasterSpec();
	}, [fetchDataListMasterSpec]);

	const onFinish = async (values) => {
		setIsLoading(true);
		const res = await postDataListMasterSpec(values).catch((err) => {
			modal.error({
				title: 'Failed Message!',
				content: (
					<>
						<p>
							{err.response.data.errors.material_group &&
								err.response.data.errors.material_group[0]}
						</p>
						<p>
							{err.response.data.errors.material_group_description &&
								err.response.data.errors.material_group_description[0]}
						</p>
					</>
				),
				onOk: () => {
					form.resetFields();
				},
				onCancel: () => {
					form.resetFields();
				},
			});
			setIsLoading(false);
		});

		if (res && res.status === 200) {
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					form.resetFields();
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
					fetchDataListMasterSpec();
				},
			});
			setIsLoading(false);
		}
	};

	const onEdit = async (values) => {
		setIsLoading(true);
		const res = await putDataListMasterSpec(dataRow.id, values).catch((err) => {
			modal.error({
				title: 'Failed Message!',
				content: <p>{err.response.data.message}</p>,
			});
			setIsLoading(false);
		});

		if (res && res.status === 200) {
			modal.success({
				title: 'Success Message!',
				content: <p>Success update master spec!</p>,
				onOk: () => {
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
					fetchDataListMasterSpec();
				},
			});
		}
	};

	const onDelete = async (values) => {
		setIsLoading(true);
		const res = await deleteDataListMasterSpec(values).catch((err) => {
			modal.error({
				title: 'Failed Message!',
				content: <p>{err.response.data.message}</p>,
				onOk: () => {
					form.resetFields();
				},
				onCancel: () => {
					form.resetFields();
				},
			});
			setIsLoading(false);
		});

		if (res && res.status === 200) {
			setIsLoading(false);
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			fetchDataListMasterSpec();
		}
	};

	const columnsTable = [
		{
			title: 'No',
			dataIndex: 'material_group',
			key: 'material_group',
			render: (text, rowData, index) => <p>{index + 1}</p>,
		},
		{
			title: 'Material Group',
			dataIndex: 'material_group',
			key: 'material_group',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Group Description',
			dataIndex: 'material_group_description',
			key: 'material_group_description',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Priority ID',
			dataIndex: 'priority_id',
			key: 'priority_id',
			render: (text) => <p>{dataPriority && dataPriority[text]}</p>,
		},
		{
			title: 'ID',
			dataIndex: 'id',
			key: 'id',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Created at',
			dataIndex: 'created_at',
			key: 'created_at',
			render: (text) => <p>{moment(text).format('YYYY-MM-DD')}</p>,
		},
		{
			title: 'Update at',
			dataIndex: 'updated_at',
			key: 'updated_at',
			render: (text) => <p>{moment(text).format('YYYY-MM-DD')}</p>,
		},
		{
			title: 'Action',
			key: 'action',
			render: (text, record) => {
				return (
					<Space size='middle'>
						<Button
							onClick={() => {
								if (record.material_group_description === 'Roll Film') {
									props.history.push('/master-specification-roll', record);
								}
								if (record.material_group_description === 'Carton Box') {
									props.history.push(
										'/master-specification-cartoon-box',
										record
									);
								} else {
									setIsModalActionVisible({
										isShowModalAction: true,
										dataRow: record,
										typeAction: 'VIEW',
									});
								}
							}}
							type='primary'
							icon={<UnorderedListOutlined />}>
							List
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'EDIT',
								})
							}
							type='default'
							icon={<EditOutlined />}>
							Edit
						</Button>
						{!(
							record?.material_group_description === 'Roll Film' ||
							record?.material_group_description === 'Carton Box' ||
							record?.material_group_description === 'PP'
						) && (
							<Button
								onClick={() =>
									setIsModalActionVisible({
										...isModalActionVisible,
										isShowModalAction: true,
										dataRow: record,
										typeAction: 'DELETE',
									})
								}
								type='danger'
								icon={<DeleteOutlined />}>
								Delete
							</Button>
						)}
					</Space>
				);
			},
		},
	];

	const onSearch = (data) => {
		if (data === '') {
			fetchDataListMasterSpec();
		} else {
			let tempData = dataListMasterSpec.filter(
				(filtering) =>
					filtering.material_group_description.toLowerCase() ===
					data.toLowerCase()
			);

			setDataListMasterSpec(tempData);
		}
	};

	return (
		<Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
			<Title level={4}>Master Specification</Title>
			<div className='site-layout-background' style={{ padding: 24 }}>
				<Row
					justify='space-between'
					style={{
						marginBottom: 12,
					}}>
					<Button
						onClick={() =>
							setIsModalActionVisible({
								...isModalActionVisible,
								typeAction: 'ADD',
								isShowModalAction: true,
							})
						}
						style={{
							backgroundColor: ColorPrimaryEnum.greenTosca,
							border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
						}}
						type='primary'
						icon={<FileAddOutlined />}>
						Add
					</Button>
					<Input.Search
						placeholder='wajib masukkan nama lengkap untuk material group desc.'
						onSearch={onSearch}
						allowClear
						oncle
						style={{ width: '40%' }}
					/>
				</Row>
				<Table
					loading={isLoading}
					bordered
					rowClassName={(record, index) => {
						if (index % 2 === 1) {
							return 'color-gray-2';
						} else {
							return 'color-gray-1';
						}
					}}
					columns={columnsTable.filter((col) => col.dataIndex !== 'id')}
					dataSource={dataListMasterSpec}
				/>
			</div>

			<Modal
				title={typeAction}
				forceRender={false}
				visible={isShowModalAction}
				width={typeAction === 'ADD' || typeAction === 'EDIT' ? 1000 : undefined}
				afterClose={() => form.resetFields()}
				onOk={() => {
					if (typeAction === 'DELETE') {
						if (isLoading) {
							return;
						} else {
							onDelete(dataRow.id);
						}
					} else {
						form.resetFields();
						setIsModalActionVisible({
							...isModalActionVisible,
							isShowModalAction: false,
						});
					}
				}}
				onCancel={() => {
					form.resetFields();
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
				}}>
				{typeAction === 'VIEW' ? (
					dataRow &&
					Object.keys(dataRow).map((data, index) => {
						if (data === 'key' || data === 'guid') {
							return null;
						}
						return (
							<p key={index} style={{ fontWeight: 'bold', color: '#595959' }}>
								{data.replace('_', ' ')} :{' '}
								<span style={{ fontWeight: 'normal' }}>
									{data === 'created_at' || data === 'updated_at'
										? moment(dataRow[data]).format('YYYY-MM-DD')
										: dataRow[data]}
								</span>
							</p>
						);
					})
				) : typeAction === 'EDIT' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='add'
						onFinish={onEdit}
						fields={[
							{
								name: ['material_group'],
								value: dataRow.material_group,
							},
							{
								name: ['material_group_description'],
								value: dataRow.material_group_description,
							},
							{
								name: ['priority_id'],
								value: dataPriority[dataRow.priority_id],
							},
						]}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='material_group'
									label='Material Group'
									rules={[
										{
											required: true,
											message: 'Please input material group!',
										},
									]}
									hasFeedback>
									<Input type='number' />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='material_group_description'
									label='Material Group Desc.'
									rules={[
										{
											required: true,
											message: 'Please input material group desc. !',
										},
									]}
									hasFeedback>
									<Input
										disabled={
											dataRow.material_group_description === 'Roll Film' ||
											dataRow.material_group_description === 'Carton Box' ||
											dataRow.material_group_description === 'PP'
										}
									/>
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='priority_id'
									label='Priority'
									rules={[
										{
											required: true,
											message: 'Please input Priority!',
										},
									]}
									hasFeedback>
									<Select
										loading={isLoading}
										placeholder='select your Priority'>
										{dataPriority &&
											Object.keys(dataPriority).map((data, index) => {
												return (
													<Option key={index} value={data}>
														{dataPriority[data]}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<Form.Item hidden name='guid' label='guid'>
							<Input />
						</Form.Item>
						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Save
								</Button>
							</Form.Item>
						</div>
					</Form>
				) : typeAction === 'ADD' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='addMasterSpec'
						onFinish={onFinish}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='material_group'
									label='Material Group'
									rules={[
										{
											required: true,
											message: 'Please input material group!',
										},
									]}
									hasFeedback>
									<Input type='number' />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='material_group_description'
									label='Material Group Desc.'
									rules={[
										{
											required: true,
											message: 'Please input material group desc. !',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='priority_id'
									label='Priority'
									rules={[
										{
											required: true,
											message: 'Please input Priority!',
										},
									]}
									hasFeedback>
									<Select
										loading={isLoading}
										placeholder='select your Priority'>
										{dataPriority &&
											Object.keys(dataPriority).map((data, index) => {
												return (
													<Option key={index} value={data}>
														{dataPriority[data]}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Create
								</Button>
							</Form.Item>
						</div>
					</Form>
				) : (
					<p>
						Apakah anda ingin menghapus daata{' '}
						<b>{dataRow?.material_group_description}</b> ini ?
					</p>
				)}
			</Modal>
			{contextHolder}
		</Content>
	);
};

export default MasterSpesification;
