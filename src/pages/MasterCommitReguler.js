import {
	CloudDownloadOutlined,
	CloudUploadOutlined,
	DeleteOutlined,
	EditOutlined,
	EyeOutlined,
	FileAddOutlined,
	InboxOutlined,
	UploadOutlined,
} from '@ant-design/icons';
import {
	Button,
	Col,
	Form,
	Input,
	Layout,
	Modal,
	Row,
	Select,
	Space,
	Table,
	Typography,
	Upload,
} from 'antd';
import React, { useState } from 'react';
import { ColorBaseEnum, ColorPrimaryEnum } from '../styles/Colors';
import { MarginEnum } from '../styles/Spacer';

const { Content } = Layout;
const { Option } = Select;
const { Title } = Typography;

const formItemLayout = {
	labelCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 8,
		},
	},
	wrapperCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 16,
		},
	},
};

const MasterCommitReguler = (props) => {
	const [modal, contextHolder] = Modal.useModal();
	const [form] = Form.useForm();
	const [isModalActionVisible, setIsModalActionVisible] = useState({
		dataRow: null,
		typeAction: '',
		isShowModalAction: false,
	});
	const [isLoading, setIsLoading] = useState(false);
	const { isShowModalAction, typeAction, dataRow } = isModalActionVisible;

	const normFile = (e: any) => {
		console.log('Upload event:', e);
		if (Array.isArray(e)) {
			return e;
		}
		return e && e.fileList;
	};

	const config = {
		title: 'Success Message!',
		content: <p>Success create supplier!</p>,
		onOk: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			window.location.reload();
		},
		onCancel: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
		},
	};

	const updateSupllier = {
		title: 'Success Message!',
		content: <p>Success update supplier!</p>,
		onOk: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			window.location.reload();
		},
		onCancel: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
		},
	};

	const onFinish = async (values) => {
		setIsLoading(true);
		modal.confirm('add');
		setIsLoading(false);
	};

	const onEditSupplier = async (values) => {
		setIsLoading(true);
		modal.confirm('udpate');
		setIsLoading(false);
	};

	const onDeleteSupplier = async (values) => {
		window.alert('Success delete supplier');
		setIsModalActionVisible({
			...isModalActionVisible,
			isShowModalAction: false,
		});
		window.location.reload();
	};

	const dataTable = [
		{
			key: 1,
			company: 'KSNI',
			vendor: '200012',
			vendorDesc: 'Century MMS PT',
			materialNo: '2014600003',
			materialDesc: 'ROL AHH RCO 17.5G (D1-THA)',
			ratio: '25%',
		},
	];

	const columnsTable = [
		{
			title: 'No',
			dataIndex: 'key',
			key: 'key',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Company',
			dataIndex: 'company',
			key: 'company',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Vendor',
			dataIndex: 'vendor',
			key: 'vendor',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Vendor Description',
			dataIndex: 'vendorDesc',
			key: 'vendorDesc',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material No',
			dataIndex: 'materialNo',
			key: 'materialNo',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Description',
			dataIndex: 'materialDesc',
			key: 'materialDesc',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Ratio',
			dataIndex: 'ratio',
			key: 'ratio',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Action',
			key: 'action',
			render: (text, record) => {
				return (
					<Space size='middle'>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'VIEW',
								})
							}
							type='primary'
							icon={<EyeOutlined />}>
							View
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'EDIT',
								})
							}
							type='default'
							icon={<EditOutlined />}>
							Edit
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									...isModalActionVisible,
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'DELETE',
								})
							}
							type='danger'
							icon={<DeleteOutlined />}>
							Delete
						</Button>
					</Space>
				);
			},
		},
	];

	return (
		<Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
			<Title level={4}>MD Commitment Reguler</Title>
			<div className='site-layout-background' style={{ padding: 24 }}>
				<Row
					justify='end'
					style={{
						marginBottom: 12,
					}}>
					<Space>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									...isModalActionVisible,
									typeAction: 'ADD',
									isShowModalAction: true,
								})
							}
							style={{
								backgroundColor: ColorPrimaryEnum.greenTosca,
								border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
							}}
							type='primary'
							icon={<FileAddOutlined />}>
							Add
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									...isModalActionVisible,
									typeAction: 'UPLOAD',
									isShowModalAction: true,
								})
							}
							style={{
								backgroundColor: ColorPrimaryEnum.redVelvet,
								border: `1px solid ${ColorPrimaryEnum.redVelvet}`,
							}}
							type='primary'
							icon={<CloudUploadOutlined />}>
							Upload
						</Button>
						<Button
							style={{
								backgroundColor: ColorPrimaryEnum.redVelvet,
								border: `1px solid ${ColorPrimaryEnum.redVelvet}`,
							}}
							type='primary'
							icon={<CloudDownloadOutlined />}>
							Download
						</Button>
					</Space>
				</Row>
				<Table
					loading={isLoading}
					bordered
					columns={columnsTable}
					dataSource={dataTable}
					scroll={{ x: 1500 }}
				/>
			</div>

			<Modal
				title={typeAction}
				forceRender={false}
				visible={isShowModalAction}
				width={typeAction === 'ADD' || typeAction === 'EDIT' ? 1000 : undefined}
				afterClose={() => form.resetFields()}
				onOk={() => {
					if (typeAction === 'DELETE') {
						onDeleteSupplier(dataRow.guid);
					} else {
						form.resetFields();
						setIsModalActionVisible({
							...isModalActionVisible,
							isShowModalAction: false,
						});
					}
				}}
				onCancel={() => {
					form.resetFields();
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
				}}>
				{typeAction === 'VIEW' ? (
					dataRow &&
					Object.keys(dataRow).map((data, index) => {
						if (
							data === 'key' ||
							data === 'guid' ||
							data === 'updated_at' ||
							data === 'created_at'
						) {
							return null;
						}
						return (
							<p key={index} style={{ fontWeight: 'bold', color: '#595959' }}>
								{data.replace('_', ' ')} :{' '}
								<span style={{ fontWeight: 'normal' }}>{dataRow[data]}</span>
							</p>
						);
					})
				) : typeAction === 'EDIT' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='addSupplier'
						onFinish={onEditSupplier}
						fields={[
							{
								name: ['company'],
								value: dataRow.company,
							},
							{
								name: ['vendor'],
								value: dataRow.vendor,
							},
							{
								name: ['vendorDesc'],
								value: dataRow.vendorDesc,
							},
							{
								name: ['materialNo'],
								value: dataRow.materialNo,
							},
							{
								name: ['materialDesc'],
								value: dataRow.materialDesc,
							},
							{
								name: ['ratio'],
								value: dataRow.ratio,
							},
							{
								name: ['status'],
								value: dataRow.status,
							},
						]}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='company'
									label='Company'
									rules={[
										{
											required: true,
											message: 'Please input company!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange company ${e}`)}
										onFocus={(e) => console.log(`onFocus company ${e}`)}
										onBlur={(e) => console.log(`onBlur company ${e}`)}
										onSearch={(e) => console.log(`onSearch company ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='vendor'
									label='Vendor'
									rules={[
										{
											required: true,
											message: 'Please input vendor!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange vendor ${e}`)}
										onFocus={(e) => console.log(`onFocus vendor ${e}`)}
										onBlur={(e) => console.log(`onBlur vendor ${e}`)}
										onSearch={(e) => console.log(`onSearch vendor ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='materialNo'
									label='Material'
									rules={[
										{
											required: true,
											message: 'Please input material!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange material ${e}`)}
										onFocus={(e) => console.log(`onFocus material ${e}`)}
										onBlur={(e) => console.log(`onBlur material ${e}`)}
										onSearch={(e) => console.log(`onSearch material ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='ratio'
									label='Ratio'
									rules={[
										{
											required: true,
											message: 'Please input ratio!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>

						<Form.Item hidden name='guid' label='guid'>
							<Input />
						</Form.Item>
						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Save
								</Button>
							</Form.Item>
							{contextHolder}
						</div>
					</Form>
				) : typeAction === 'ADD' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='addSupplier'
						onFinish={onFinish}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='company'
									label='Company'
									rules={[
										{
											required: true,
											message: 'Please input company!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange company ${e}`)}
										onFocus={(e) => console.log(`onFocus company ${e}`)}
										onBlur={(e) => console.log(`onBlur company ${e}`)}
										onSearch={(e) => console.log(`onSearch company ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='vendor'
									label='Vendor'
									rules={[
										{
											required: true,
											message: 'Please input vendor!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange vendor ${e}`)}
										onFocus={(e) => console.log(`onFocus vendor ${e}`)}
										onBlur={(e) => console.log(`onBlur vendor ${e}`)}
										onSearch={(e) => console.log(`onSearch vendor ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='materialNo'
									label='Material'
									rules={[
										{
											required: true,
											message: 'Please input material!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange material ${e}`)}
										onFocus={(e) => console.log(`onFocus material ${e}`)}
										onBlur={(e) => console.log(`onBlur material ${e}`)}
										onSearch={(e) => console.log(`onSearch material ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='ratio'
									label='Ratio'
									rules={[
										{
											required: true,
											message: 'Please input ratio!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>

						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Create
								</Button>
							</Form.Item>
							{contextHolder}
						</div>
					</Form>
				) : typeAction === 'UPLOAD' ? (
					<>
						<Form.Item>
							<Form.Item
								name='uploadFileMdDesign'
								valuePropName='fileList'
								getValueFromEvent={normFile}
								noStyle>
								<Upload.Dragger name='files' action='/upload.do'>
									<p className='ant-upload-drag-icon'>
										<InboxOutlined />
									</p>
									<p className='ant-upload-text'>
										Click or drag file to this area to upload
									</p>
									<p className='ant-upload-hint'>
										Support for a single or bulk upload.
									</p>
								</Upload.Dragger>
							</Form.Item>
						</Form.Item>

						<Button
							block
							style={{
								backgroundColor: ColorBaseEnum.white,
								color: ColorPrimaryEnum.redVelvet,
								border: `1px solid ${ColorPrimaryEnum.redVelvet}`,
								marginBottom: MarginEnum['2x'],
							}}
							type='primary'
							icon={<UploadOutlined />}>
							Upload File
						</Button>
						<Button
							block
							style={{
								backgroundColor: ColorBaseEnum.white,
								color: ColorPrimaryEnum.greenTosca,
								border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
							}}
							type='primary'
							icon={<FileAddOutlined />}>
							Download Template
						</Button>
					</>
				) : (
					<p>Apakah anda ingin menghapus data ini ?</p>
				)}
			</Modal>
		</Content>
	);
};

export default MasterCommitReguler;
