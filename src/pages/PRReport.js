import { DownloadOutlined, EyeOutlined } from '@ant-design/icons';
import {
	Button,
	DatePicker,
	Layout,
	Row,
	Select,
	Space,
	Table,
	Typography,
} from 'antd';
import React, { useState } from 'react';
import { ColorBaseEnum, ColorPrimaryEnum } from '../styles/Colors';
import { MarginEnum } from '../styles/Spacer';

const { Content } = Layout;
const { Option } = Select;
const { Title, Text } = Typography;

const PRReport = (props) => {
	const [isLoading, setIsLoading] = useState(false);

	const dataTable = [
		{
			key: 1,
			plant: 'K104',
			materialNo: '2011100017',
			materialDesc: 'ROL NABATI 145G EXPORT',
			baseUnit: 'PC',
			qtyPR: '300',
		},
	];

	const columnsTable = [
		{
			title: 'No',
			dataIndex: 'key',
			key: 'key',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Plant',
			dataIndex: 'plant',
			key: 'plant',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material No',
			dataIndex: 'materialNo',
			key: 'materialNo',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Description',
			dataIndex: 'materialDesc',
			key: 'materialDesc',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Base Unit',
			dataIndex: 'baseUnit',
			key: 'baseUnit',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Qty PR',
			dataIndex: 'qtyPR',
			key: 'qtyPR',
			render: (text) => <p>{text}</p>,
		},
	];

	return (
		<Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
			<Title level={4}>PR Report</Title>
			<div className='site-layout-background' style={{ padding: 24 }}>
				<Row
					style={{
						marginBottom: MarginEnum['2x'],
					}}>
					<Space>
						<Space direction='vertical'>
							<Text>Period From</Text>
							<DatePicker />
						</Space>
						<Space direction='vertical'>
							<Text>Period To</Text>
							<DatePicker />
						</Space>
						<Space direction='vertical'>
							<Text>Version Date</Text>
							<DatePicker />
						</Space>
						<Space direction='vertical'>
							<Text>Company</Text>
							<Select
								allowClear
								showSearch
								style={{ width: 200 }}
								placeholder='Select'
								optionFilterProp='children'
								onChange={(e) => console.log(`onChange company ${e}`)}
								onFocus={(e) => console.log(`onFocus company ${e}`)}
								onBlur={(e) => console.log(`onBlur company ${e}`)}
								onSearch={(e) => console.log(`onSearch company ${e}`)}
								filterOption={(input, option) =>
									option.children.toLowerCase().indexOf(input.toLowerCase()) >=
									0
								}>
								<Option value='jack'>Jack</Option>
								<Option value='lucy'>Lucy</Option>
								<Option value='tom'>Tom</Option>
							</Select>
						</Space>
						<Space direction='vertical'>
							<Text style={{ color: ColorBaseEnum.white }}>text</Text>
							<Button
								style={{
									backgroundColor: ColorPrimaryEnum.redVelvet,
									border: `1px solid ${ColorPrimaryEnum.redVelvet}`,
								}}
								type='primary'
								icon={<EyeOutlined />}>
								View
							</Button>
						</Space>
						<Space direction='vertical'>
							<Text style={{ color: ColorBaseEnum.white }}>text</Text>
							<Button
								style={{
									backgroundColor: ColorPrimaryEnum.greenTosca,
									border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
								}}
								type='primary'
								icon={<DownloadOutlined />}>
								Download
							</Button>
						</Space>
					</Space>
				</Row>
				<Table
					loading={isLoading}
					bordered
					columns={columnsTable}
					dataSource={dataTable}
					scroll={{ x: 1500 }}
				/>
			</div>
		</Content>
	);
};

export default PRReport;
