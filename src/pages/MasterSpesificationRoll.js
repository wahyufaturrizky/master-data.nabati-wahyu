/* eslint-disable react-hooks/exhaustive-deps */
import {
	ArrowLeftOutlined,
	DeleteOutlined,
	EditOutlined,
	FileAddOutlined,
} from '@ant-design/icons';
import {
	Button,
	Col,
	Form,
	Input,
	Layout,
	Modal,
	Radio,
	Row,
	Select,
	Space,
	Table,
	Tabs,
	Typography,
} from 'antd';
import moment from 'moment';
import React, { useCallback, useEffect, useState } from 'react';
import {
	deleteDataListMasterSpecRoll,
	getDataListCharacteritic,
	getDataListMasterSpecRoll,
	postDataListMasterSpecRoll,
	postLogin,
	putDataListMasterSpecRoll,
} from '../services/retrieveData';
import { ColorPrimaryEnum } from '../styles/Colors';

const { Content } = Layout;
const { Option } = Select;
const { Title } = Typography;
const { TabPane } = Tabs;

const formItemLayout = {
	labelCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 8,
		},
	},
	wrapperCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 16,
		},
	},
};

const MasterSpesificationRoll = (props) => {
	const [modal, contextHolder] = Modal.useModal();
	const [form] = Form.useForm();
	const [dataListMasterSpec, setDataListMasterSpec] = useState([]);
	const [dataCharacter, setDataCharacter] = useState([]);
	const [dataGroup, setDataGroup] = useState(null);
	const [isModalActionVisible, setIsModalActionVisible] = useState({
		dataRow: null,
		typeAction: '',
		isShowModalAction: false,
	});
	const [isLoading, setIsLoading] = useState(false);
	const { isShowModalAction, typeAction, dataRow } = isModalActionVisible;

	const fetchDataListMasterSpec = useCallback(async ({ keyword }) => {
		setIsLoading(true);
		const data = {
			search: keyword || '',
			material_group: props.location.state.material_group,
		};

		const res = await getDataListMasterSpecRoll(data).catch((err) => {
			setIsLoading(false);
			if (err?.response?.data?.message === 'Unauthenticated.') {
				modal.warning({
					title: 'Warning Message!',
					content: (
						<p>Your session has expired, click OK to renew your session</p>
					),
					onOk: async () => {
						setIsLoading(true);
						const dataLogin = await localStorage.getItem('dataUser');
						const dataLoginParse = JSON.parse(dataLogin);
						let data = new FormData();

						data.append('username', dataLoginParse.username || 'user_demo');
						data.append('password', dataLoginParse.password || '12345');
						const res = await postLogin(data).catch((err) => {
							window.alert('Failed login');
							setIsLoading(false);
						});

						if (res && res.status === 200) {
							if (res.data === '') {
								setIsLoading(false);
								modal.error({
									title: 'Failed login!',
									content: (
										<p>Username/password is not correct, please try again</p>
									),
									onOk: () => {},
								});
							} else {
								localStorage.setItem('dataUser', JSON.stringify(res.data));
								fetchDataListMasterSpec({});
								setIsLoading(false);
							}
						}
					},
				});
			} else {
				modal.error({
					title: 'Failed Message!',
					content: (
						<p>{err?.response?.data?.message || 'Internal server error'}</p>
					),
					onOk: () => {},
				});
			}
		});

		if (res && res.status === 200) {
			setIsLoading(false);
			setDataListMasterSpec(res.data.items.data);
			setDataGroup(res.data.groups);
		}
	}, []);

	useEffect(() => {
		const fetchDataLisCharacteritic = async () => {
			setIsLoading(true);
			const data = {
				material_group_id: props.location.state.material_group,
				search: '',
			};

			const res = await getDataListCharacteritic(data).catch((err) => {
				setIsLoading(false);
				if (err?.response?.data?.message === 'Unauthenticated.') {
					modal.warning({
						title: 'Warning Message!',
						content: (
							<p>Your session has expired, click OK to renew your session</p>
						),
						onOk: async () => {
							setIsLoading(true);
							const dataLogin = await localStorage.getItem('dataUser');
							const dataLoginParse = JSON.parse(dataLogin);
							let data = new FormData();

							data.append('username', dataLoginParse.username || 'user_demo');
							data.append('password', dataLoginParse.password || '12345');
							const res = await postLogin(data).catch((err) => {
								window.alert('Failed login');
								setIsLoading(false);
							});

							if (res && res.status === 200) {
								if (res.data === '') {
									setIsLoading(false);
									modal.error({
										title: 'Failed login!',
										content: (
											<p>Username/password is not correct, please try again</p>
										),
										onOk: () => {},
									});
								} else {
									localStorage.setItem('dataUser', JSON.stringify(res.data));
									fetchDataListMasterSpec({});
									setIsLoading(false);
								}
							}
						},
					});
				} else {
					modal.error({
						title: 'Failed Message!',
						content: (
							<p>{err?.response?.data?.message || 'Internal server error'}</p>
						),
						onOk: () => {},
					});
				}
			});

			if (res && res.status === 200) {
				setIsLoading(false);
				setDataCharacter(res.data.items);
			}
		};

		fetchDataListMasterSpec({});
		fetchDataLisCharacteritic();
	}, [fetchDataListMasterSpec]);

	const onFinish = async (values) => {
		setIsLoading(true);
		let data = {
			characteristic_id: values.characteristic_id,
			specification_id: props.location.state.id,
			group_id: parseInt(values.group_id),
			characteristic_description: values.characteristic_description,
			range: values.range,
		};

		const res = await postDataListMasterSpecRoll(data).catch((err) => {
			modal.error({
				title: 'Failed Message!',
				content: <p>{err.response.data.message || 'Internal server error'}</p>,
				onOk: () => {
					form.resetFields();
				},
				onCancel: () => {
					form.resetFields();
				},
			});
			setIsLoading(false);
		});

		if (res && res.status === 200) {
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					form.resetFields();
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
					fetchDataListMasterSpec({});
				},
			});
			fetchDataListMasterSpec({});
		}
	};

	const onEdit = async (values) => {
		setIsLoading(true);

		let data = {
			characteristic_id: values.characteristic_id,
			specification_id: props.location.state.id,
			group_id: parseInt(values.group_id),
			characteristic_description: values.characteristic_description,
			range: values.range,
		};

		const res = await putDataListMasterSpecRoll(dataRow.id, data).catch(
			(err) => {
				modal.error({
					title: 'Failed Message!',
					content: <p>{err.response.data.message}</p>,
				});
				setIsLoading(false);
			}
		);

		if (res && res.status === 200) {
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
					fetchDataListMasterSpec({});
				},
			});
		}
	};

	const onDelete = async (values) => {
		setIsLoading(true);
		const res = await deleteDataListMasterSpecRoll(values).catch((err) => {
			modal.error({
				title: 'Failed Message!',
				content: <p>{err.response.data.message || 'Internal server error'}</p>,
				onOk: () => {
					form.resetFields();
				},
			});
			setIsLoading(false);
		});

		if (res && res.status === 200) {
			setIsLoading(false);
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					form.resetFields();
					fetchDataListMasterSpec({});
				},
			});
		}
	};

	let columnsTable = [
		{
			title: 'No',
			dataIndex: 'id',
			key: 'id',
			render: (text, rowData, index) => <p>{index + 1}</p>,
		},
		{
			title: 'Characteristic',
			dataIndex: 'characteristic',
			key: 'characteristic',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Characteristic ID',
			dataIndex: 'characteristic_id',
			key: 'characteristic_id',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Specification ID',
			dataIndex: 'specification_id',
			key: 'specification_id',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Group',
			dataIndex: 'material_group',
			key: 'material_group',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Group Desc',
			dataIndex: 'material_group_description',
			key: 'material_group_description',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Group Name',
			dataIndex: 'group_name',
			key: 'group_name',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Characteristic Desc',
			dataIndex: 'characteristic_description',
			key: 'characteristic_description',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Range',
			dataIndex: 'range',
			key: 'range',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'State',
			dataIndex: 'state',
			key: 'state',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Type',
			dataIndex: 'type',
			key: 'type',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Created',
			dataIndex: 'created_at',
			key: 'created_at',
			render: (text) => <p>{moment(text).format('YYYY-MM-DD')}</p>,
		},
		{
			title: 'Updated',
			dataIndex: 'updated_at',
			key: 'updated_at',
			render: (text) => <p>{moment(text).format('YYYY-MM-DD')}</p>,
		},
		{
			title: 'Group ID',
			dataIndex: 'group_id',
			key: 'group_id',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'UUID',
			dataIndex: 'uuid',
			key: 'uuid',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Action',
			key: 'action',
			render: (text, record) => {
				return (
					<Space size='middle'>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'EDIT',
								})
							}
							type='default'
							icon={<EditOutlined />}>
							Edit
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									...isModalActionVisible,
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'DELETE',
								})
							}
							type='danger'
							icon={<DeleteOutlined />}>
							Delete
						</Button>
					</Space>
				);
			},
		},
	];

	const onSearch = (data) => {
		fetchDataListMasterSpec({ keyword: data });
	};

	return (
		<Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
			<Title level={4}>Master Specification</Title>
			<div className='site-layout-background' style={{ padding: 24 }}>
				<Row justify='space-between'>
					<Title level={4}>
						{props?.location?.state?.material_group} -{' '}
						{props?.location?.state?.material_group_description}
					</Title>
					<Button
						onClick={() => props.history.push('/master-specification')}
						type='default'
						icon={<ArrowLeftOutlined />}>
						Back to list
					</Button>
				</Row>
				<Tabs defaultActiveKey='1'>
					<TabPane tab='Specification' key='1'>
						<Row
							justify='space-between'
							style={{
								marginBottom: 12,
							}}>
							<Button
								onClick={() =>
									setIsModalActionVisible({
										...isModalActionVisible,
										typeAction: 'ADD',
										isShowModalAction: true,
									})
								}
								style={{
									backgroundColor: ColorPrimaryEnum.greenTosca,
									border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
								}}
								type='primary'
								icon={<FileAddOutlined />}>
								Add
							</Button>
							<Input.Search
								onSearch={onSearch}
								allowClear
								style={{ width: '40%' }}
							/>
						</Row>
						<Table
							loading={isLoading}
							bordered
							rowClassName={(record, index) => {
								if (index % 2 === 1) {
									return 'color-gray-2';
								} else {
									return 'color-gray-1';
								}
							}}
							columns={columnsTable.filter(
								(col) =>
									col.dataIndex !== 'uuid' &&
									col.dataIndex !== 'created_at' &&
									col.dataIndex !== 'updated_at' &&
									col.dataIndex !== 'characteristic_id' &&
									col.dataIndex !== 'group_id' &&
									col.dataIndex !== 'state' &&
									col.dataIndex !== 'type' &&
									col.dataIndex !== 'material_group' &&
									col.dataIndex !== 'specification_id'
							)}
							dataSource={dataListMasterSpec}
						/>
					</TabPane>
				</Tabs>
			</div>

			<Modal
				title={typeAction}
				forceRender={false}
				visible={isShowModalAction}
				width={typeAction === 'ADD' || typeAction === 'EDIT' ? 1000 : undefined}
				afterClose={() => form.resetFields()}
				onOk={() => {
					if (typeAction === 'DELETE') {
						if (isLoading) {
							return;
						} else {
							onDelete(dataRow.id);
						}
					} else {
						form.resetFields();
						setIsModalActionVisible({
							...isModalActionVisible,
							isShowModalAction: false,
						});
					}
				}}
				onCancel={() => {
					form.resetFields();
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
				}}>
				{typeAction === 'VIEW' ? (
					dataRow &&
					Object.keys(dataRow).map((data, index) => {
						if (data === 'key' || data === 'guid') {
							return null;
						}
						return (
							<p key={index} style={{ fontWeight: 'bold', color: '#595959' }}>
								{data.replace('_', ' ')} :{' '}
								<span style={{ fontWeight: 'normal' }}>{dataRow[data]}</span>
							</p>
						);
					})
				) : typeAction === 'EDIT' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='edit'
						onFinish={onEdit}
						fields={[
							{
								name: ['characteristic_id'],
								value: dataRow.characteristic_id,
							},
							{
								name: ['group_id'],
								value: dataRow.group_id,
							},
							{
								name: ['characteristic_description'],
								value: dataRow.characteristic_description,
							},
							{
								name: ['range'],
								value: dataRow.range,
							},
						]}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='characteristic_id'
									label='Characteristic'
									rules={[
										{
											required: true,
											message: 'Please input your characteristic!',
										},
									]}
									hasFeedback>
									<Select
										loading={isLoading}
										placeholder='select your characteristic'>
										{dataCharacter &&
											dataCharacter.map((data, index) => {
												const { id, char_code_variable } = data;
												return (
													<Option key={index} value={id}>
														{char_code_variable}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='group_id'
									label='Group'
									help='1 = Warna, 2 = Bahan, 3 = Flute'
									rules={[
										{
											required: true,
											message: 'Please input group!',
										},
									]}
									hasFeedback>
									<Select loading={isLoading} placeholder='select your group'>
										{dataGroup &&
											Object.keys(dataGroup).map((data, index) => {
												return (
													<Option key={index} value={data}>
														{dataGroup[data]}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='characteristic_description'
									label='Characteristic desc.'
									rules={[
										{
											required: true,
											message: 'Please input your characteristic desc.!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='range'
									label='Range'
									rules={[
										{
											required: true,
											message: 'Please input range!',
										},
									]}
									hasFeedback>
									<Radio.Group>
										<Radio value={'Yes'}>Yes</Radio>
										<Radio value={'No'}>No</Radio>
									</Radio.Group>
								</Form.Item>
							</Col>
						</Row>

						<Form.Item hidden name='guid' label='guid'>
							<Input />
						</Form.Item>
						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Save
								</Button>
							</Form.Item>
						</div>
					</Form>
				) : typeAction === 'ADD' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='add'
						onFinish={onFinish}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='characteristic_id'
									label='Characteristic'
									rules={[
										{
											required: true,
											message: 'Please input your characteristic!',
										},
									]}
									hasFeedback>
									<Select
										loading={isLoading}
										placeholder='select your characteristic'>
										{dataCharacter &&
											dataCharacter.map((data, index) => {
												const { id, char_code_variable } = data;
												return (
													<Option key={index} value={id}>
														{char_code_variable}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='group_id'
									label='Group'
									rules={[
										{
											required: true,
											message: 'Please input group!',
										},
									]}
									hasFeedback>
									<Select loading={isLoading} placeholder='select your group'>
										{dataGroup &&
											Object.keys(dataGroup).map((data, index) => {
												return (
													<Option key={index} value={data}>
														{dataGroup[data]}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='characteristic_description'
									label='Characteristic desc.'
									rules={[
										{
											required: true,
											message: 'Please input your characteristic desc.!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='range'
									label='Range'
									rules={[
										{
											required: true,
											message: 'Please input range!',
										},
									]}
									hasFeedback>
									<Radio.Group>
										<Radio value={'Yes'}>Yes</Radio>
										<Radio value={'No'}>No</Radio>
									</Radio.Group>
								</Form.Item>
							</Col>
						</Row>

						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Create
								</Button>
							</Form.Item>
						</div>
					</Form>
				) : (
					<p>
						Apakah anda ingin menghapus daata <b>{dataRow?.characteristic}</b>{' '}
						ini ?
					</p>
				)}
			</Modal>
			{contextHolder}
		</Content>
	);
};

export default MasterSpesificationRoll;
