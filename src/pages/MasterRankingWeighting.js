import {
	CloudDownloadOutlined,
	CloudUploadOutlined,
	DeleteOutlined,
	EditOutlined,
	EyeOutlined,
	FileAddOutlined,
	InboxOutlined,
} from '@ant-design/icons';
import {
	Button,
	Col,
	Form,
	Input,
	Layout,
	Modal,
	Row,
	Select,
	Space,
	Table,
	Typography,
	Upload,
} from 'antd';
import React, { useState } from 'react';
import { ColorBaseEnum, ColorPrimaryEnum } from '../styles/Colors';
import { MarginEnum } from '../styles/Spacer';

const { Content } = Layout;
const { Option } = Select;
const { Title } = Typography;

const formItemLayout = {
	labelCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 8,
		},
	},
	wrapperCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 16,
		},
	},
};

const MasterRankingWeighting = (props) => {
	const prefixSelector = (
		<Form.Item name='prefix' noStyle>
			<Select
				style={{
					width: 70,
				}}>
				<Option value='62'>+62</Option>
				<Option value='87'>+87</Option>
			</Select>
		</Form.Item>
	);

	const [modal, contextHolder] = Modal.useModal();
	const [form] = Form.useForm();
	const [isModalActionVisible, setIsModalActionVisible] = useState({
		dataRow: null,
		typeAction: '',
		isShowModalAction: false,
	});
	const [isLoading, setIsLoading] = useState(false);
	const { isShowModalAction, typeAction, dataRow } = isModalActionVisible;

	const normFile = (e: any) => {
		console.log('Upload event:', e);
		if (Array.isArray(e)) {
			return e;
		}
		return e && e.fileList;
	};

	const config = {
		title: 'Success Message!',
		content: <p>Success create supplier!</p>,
		onOk: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			window.location.reload();
		},
		onCancel: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
		},
	};

	const updateSupllier = {
		title: 'Success Message!',
		content: <p>Success update supplier!</p>,
		onOk: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			window.location.reload();
		},
		onCancel: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
		},
	};

	const onFinish = async (values) => {
		setIsLoading(true);
		modal.confirm('add');
		setIsLoading(false);
	};

	const onEditSupplier = async (values) => {
		setIsLoading(true);
		modal.confirm('udpate');
		setIsLoading(false);
	};

	const onDeleteSupplier = async (values) => {
		window.alert('Success delete supplier');
		setIsModalActionVisible({
			...isModalActionVisible,
			isShowModalAction: false,
		});
		window.location.reload();
	};

	const dataTable = [
		{
			key: 1,
			company: '10',
			price: '40',
			performance: '60',
		},
	];

	const columnsTable = [
		{
			title: 'No',
			dataIndex: 'key',
			key: 'key',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Company',
			dataIndex: 'company',
			key: 'company',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Price %',
			dataIndex: 'price',
			key: 'price',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Performance %',
			dataIndex: 'performance',
			key: 'performance',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Action',
			key: 'action',
			render: (text, record) => {
				return (
					<Space size='middle'>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'VIEW',
								})
							}
							type='primary'
							icon={<EyeOutlined />}>
							View
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'EDIT',
								})
							}
							type='default'
							icon={<EditOutlined />}>
							Edit
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									...isModalActionVisible,
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'DELETE',
								})
							}
							type='danger'
							icon={<DeleteOutlined />}>
							Delete
						</Button>
					</Space>
				);
			},
		},
	];

	return (
		<Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
			<Title level={4}>Ranking Weighting</Title>
			<div className='site-layout-background' style={{ padding: 24 }}>
				<Row
					justify='space-between'
					style={{
						marginBottom: 12,
					}}>
					<Button
						onClick={() =>
							setIsModalActionVisible({
								...isModalActionVisible,
								typeAction: 'ADD',
								isShowModalAction: true,
							})
						}
						style={{
							backgroundColor: ColorPrimaryEnum.greenTosca,
							border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
						}}
						type='primary'
						icon={<FileAddOutlined />}>
						Add
					</Button>
					<Input.Search allowClear style={{ width: '40%' }} />
				</Row>
				<Table
					loading={isLoading}
					bordered
					columns={columnsTable}
					dataSource={dataTable}
				/>
			</div>

			<Modal
				title={typeAction}
				forceRender={false}
				visible={isShowModalAction}
				width={typeAction === 'ADD' || typeAction === 'EDIT' ? 1000 : undefined}
				afterClose={() => form.resetFields()}
				onOk={() => {
					if (typeAction === 'DELETE') {
						onDeleteSupplier(dataRow.guid);
					} else {
						form.resetFields();
						setIsModalActionVisible({
							...isModalActionVisible,
							isShowModalAction: false,
						});
					}
				}}
				onCancel={() => {
					form.resetFields();
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
				}}>
				{typeAction === 'VIEW' ? (
					dataRow &&
					Object.keys(dataRow).map((data, index) => {
						if (
							data === 'key' ||
							data === 'guid' ||
							data === 'updated_at' ||
							data === 'created_at'
						) {
							return null;
						}
						return (
							<p key={index} style={{ fontWeight: 'bold', color: '#595959' }}>
								{data.replace('_', ' ')} :{' '}
								<span style={{ fontWeight: 'normal' }}>{dataRow[data]}</span>
							</p>
						);
					})
				) : typeAction === 'EDIT' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='addSupplier'
						onFinish={onEditSupplier}
						fields={[
							{
								name: ['company'],
								value: dataRow.company,
							},
							{
								name: ['price'],
								value: dataRow.price,
							},
							{
								name: ['performance'],
								value: dataRow.performance,
							},
						]}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='company'
									label='Company'
									rules={[
										{
											required: true,
											message: 'Please input company!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='price'
									label='Price'
									rules={[
										{
											required: true,
											message: 'Please input price !',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='performance'
									label='performance'
									rules={[
										{
											required: true,
											message: 'Please input perfomance!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>

						<Form.Item hidden name='guid' label='guid'>
							<Input />
						</Form.Item>
						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Save
								</Button>
							</Form.Item>
							{contextHolder}
						</div>
					</Form>
				) : typeAction === 'ADD' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='addSupplier'
						onFinish={onFinish}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='company'
									label='Company'
									rules={[
										{
											required: true,
											message: 'Please input company!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='price'
									label='Price'
									rules={[
										{
											required: true,
											message: 'Please input price !',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='performance'
									label='Performance'
									rules={[
										{
											required: true,
											message: 'Please input performance!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>

						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Create
								</Button>
							</Form.Item>
							{contextHolder}
						</div>
					</Form>
				) : (
					<p>Apakah anda ingin menghapus data ini ?</p>
				)}
			</Modal>
		</Content>
	);
};

export default MasterRankingWeighting;
