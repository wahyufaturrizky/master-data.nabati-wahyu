import {
	Button,
	Col,
	Form,
	Image,
	Input,
	Layout,
	Row,
	Typography,
	Modal,
} from 'antd';
import React, { useEffect, useState } from 'react';
import { auth } from '../services/auth';
import { postLogin } from '../services/retrieveData';
import { ColorBaseEnum, ColorPrimaryEnum } from '../styles/Colors';
import '../styles/Dashboard.css';

const { Content } = Layout;
const { Title } = Typography;

const Login = (props) => {
	const [modal, contextHolder] = Modal.useModal();
	const [isLoading, setIsLoading] = useState(false);
	useEffect(() => {
		props.history.push(auth() ? '/master-specification' : '/');
	}, [props.history]);

	const onFinish = async (values) => {
		let data = new FormData();

		data.append('username', values.username);
		data.append('password', values.password);
		setIsLoading(true);
		const res = await postLogin(data).catch((err) => {
			modal.error({
				title: 'Failed Login!',
				content: (
					<p>{err?.response?.data?.message || 'Internal server error'}</p>
				),
				onOk: () => {},
			});
			setIsLoading(false);
		});

		if (res && res.status === 200) {
			if (res.data === '') {
				setIsLoading(false);
				modal.error({
					title: 'Failed login!',
					content: <p>Username/password is not correct, please try again</p>,
					onOk: () => {},
					onCancel: () => {},
				});
			} else {
				localStorage.setItem('dataUser', JSON.stringify(res.data));
				localStorage.setItem('userAndPass', JSON.stringify(values));
				props.history.push('/master-specification');
				setIsLoading(false);
			}
		}
	};

	const onFinishFailed = (errorInfo) => {};

	return (
		<Layout style={{ backgroundColor: 'white' }}>
			<Row style={{ justifyContent: 'center' }}>
				<Col style={{ maxWidth: 450 }}>
					<Content>
						<div
							style={{
								backgroundColor: ColorPrimaryEnum.greenTosca,
								borderRadius: 10,
								paddingRight: 12,
								paddingLeft: 12,
								paddingTop: 12,
								paddingBottom: 12,
								marginTop: 24,
							}}>
							<div>
								<div className='logo'>
									<Image
										width={394 * 0.7}
										height={217.863 * 0.7}
										style={{ alignSelf: 'center' }}
										src='https://i.ibb.co/7ztPG9q/LOGO-POWERED-BY-e-DOT-e-PROC.png'
										fallback='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=='
									/>
								</div>
								<Title
									style={{ textAlign: 'center', color: ColorBaseEnum.white }}
									level={5}>
									Vendor Allocation
								</Title>
								<Form
									name='basic'
									labelCol={{
										span: 8,
									}}
									wrapperCol={{
										span: 16,
									}}
									initialValues={{
										remember: true,
									}}
									onFinish={onFinish}
									onFinishFailed={onFinishFailed}>
									<Form.Item
										colon={false}
										label={
											<label style={{ color: ColorBaseEnum.white }}>
												Username
											</label>
										}
										name='username'
										rules={[
											{
												required: true,
												message: 'Please input your username!',
											},
										]}>
										<Input />
									</Form.Item>

									<Form.Item
										colon={false}
										label={
											<label style={{ color: ColorBaseEnum.white }}>
												Password
											</label>
										}
										name='password'
										rules={[
											{
												required: true,
												message: 'Please input your password!',
											},
										]}>
										<Input.Password />
									</Form.Item>

									<Form.Item
										wrapperCol={{
											offset: 8,
											span: 16,
										}}>
										<Button
											loading={isLoading}
											type='primary'
											style={{
												backgroundColor: ColorPrimaryEnum.redVelvet,
												border: `1px solid ${ColorPrimaryEnum.redVelvet}`,
											}}
											htmlType='submit'>
											Log In
										</Button>
									</Form.Item>
								</Form>
								<p
									style={{
										textAlign: 'center',
										color: ColorBaseEnum.white,
									}}>
									EPROC @2021 Created by
									<span>
										<a
											href='https://www.nabatisnack.co.id/'
											rel='noreferrer'
											style={{ color: ColorBaseEnum.white, fontWeight: 'bold' }}
											target='_blank'>
											{' '}
											Team Dev Kaldu Sari Nabati
										</a>
									</span>
								</p>
							</div>
						</div>
					</Content>
				</Col>
			</Row>
			{contextHolder}
		</Layout>
	);
};

export default Login;
