import { DownloadOutlined, EyeOutlined } from '@ant-design/icons';
import {
	Button,
	DatePicker,
	Layout,
	Row,
	Select,
	Space,
	Table,
	Typography,
} from 'antd';
import React, { useState } from 'react';
import { ColorBaseEnum, ColorPrimaryEnum } from '../styles/Colors';
import { MarginEnum } from '../styles/Spacer';

const { Content } = Layout;
const { Option } = Select;
const { Title, Text } = Typography;

const PriceAndRanking = (props) => {
	const [isLoading, setIsLoading] = useState(false);

	const dataTable = [
		{
			key: 1,
			vendor: '200012',
			vendorDesc: 'Century MMS PT',
			materialGroup: '01',
			materialGroupDesc: 'Roll',
			bahan: 'Bahan : PET12 / VMPET12 (KZMB/TNHB) / CPP25',
			colour: '7 - 8 Warna',
			dimensi: 'Ukuran : 105mm x 150 mm x 1500 m',
			lebar: '105',
			panjang: '1.500',
			validFrom: '01-04-2021',
			validTo: '01-08-2021',
			priceUnit: '541.225',
			rfqPrice: '1',
			vendorPerformance: '100',
			rankPerformance: '1',
			scoreTotal: '100',
			rank: '1',
		},
	];

	const columnsTable = [
		{
			title: 'No',
			dataIndex: 'key',
			key: 'key',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Vendor',
			dataIndex: 'vendor',
			key: 'vendor',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Vendor Description',
			dataIndex: 'vendorDesc',
			key: 'vendorDesc',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Group',
			dataIndex: 'materialGroup',
			key: 'materialGroup',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Group Desc.',
			dataIndex: 'materialGroupDesc',
			key: 'materialGroupDesc',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Bahan',
			dataIndex: 'bahan',
			key: 'bahan',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Colour',
			dataIndex: 'colour',
			key: 'colour',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Dimensi',
			dataIndex: 'dimensi',
			key: 'dimensi',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Lebar',
			dataIndex: 'lebar',
			key: 'lebar',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Panjang',
			dataIndex: 'panjang',
			key: 'panjang',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Valid From',
			dataIndex: 'validFrom',
			key: 'validFrom',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Valid To',
			dataIndex: 'validTo',
			key: 'validTo',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'RFQ Price',
			dataIndex: 'rfqPrice',
			key: 'rfqPrice',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Price / Unit',
			dataIndex: 'priceUnit',
			key: 'priceUnit',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Ranking Price',
			dataIndex: 'rankingPrice',
			key: 'rankingPrice',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Vendor Performance',
			dataIndex: 'vendorPerformance',
			key: 'vendorPerformance',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Rank Performance',
			dataIndex: 'rankPerformance',
			key: 'rankPerformance',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Score Total',
			dataIndex: 'scoreTotal',
			key: 'scoreTotal',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Rank',
			dataIndex: 'rank',
			key: 'rank',
			render: (text) => <p>{text}</p>,
		},
	];

	return (
		<Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
			<Title level={4}>Price & Ranking Report</Title>
			<div className='site-layout-background' style={{ padding: 24 }}>
				<Row
					style={{
						marginBottom: MarginEnum['2x'],
					}}>
					<Space>
						<Space direction='vertical'>
							<Text>Valid On RFQ</Text>
							<DatePicker />
						</Space>
						<Space direction='vertical'>
							<Text>Vendor</Text>
							<Select
								allowClear
								showSearch
								style={{ width: 200 }}
								placeholder='Select'
								optionFilterProp='children'
								onChange={(e) => console.log(`onChange vendor ${e}`)}
								onFocus={(e) => console.log(`onFocus vendor ${e}`)}
								onBlur={(e) => console.log(`onBlur vendor ${e}`)}
								onSearch={(e) => console.log(`onSearch vendor ${e}`)}
								filterOption={(input, option) =>
									option.children.toLowerCase().indexOf(input.toLowerCase()) >=
									0
								}>
								<Option value='jack'>Jack</Option>
								<Option value='lucy'>Lucy</Option>
								<Option value='tom'>Tom</Option>
							</Select>
						</Space>
						<Space direction='vertical'>
							<Text>Company</Text>
							<Select
								allowClear
								showSearch
								style={{ width: 200 }}
								placeholder='Select'
								optionFilterProp='children'
								onChange={(e) => console.log(`onChange company ${e}`)}
								onFocus={(e) => console.log(`onFocus company ${e}`)}
								onBlur={(e) => console.log(`onBlur company ${e}`)}
								onSearch={(e) => console.log(`onSearch company ${e}`)}
								filterOption={(input, option) =>
									option.children.toLowerCase().indexOf(input.toLowerCase()) >=
									0
								}>
								<Option value='jack'>Jack</Option>
								<Option value='lucy'>Lucy</Option>
								<Option value='tom'>Tom</Option>
							</Select>
						</Space>
						<Space direction='vertical'>
							<Text style={{ color: ColorBaseEnum.white }}>text</Text>
							<Button
								style={{
									backgroundColor: ColorPrimaryEnum.redVelvet,
									border: `1px solid ${ColorPrimaryEnum.redVelvet}`,
								}}
								type='primary'
								icon={<EyeOutlined />}>
								View
							</Button>
						</Space>
						<Space direction='vertical'>
							<Text style={{ color: ColorBaseEnum.white }}>text</Text>
							<Button
								style={{
									backgroundColor: ColorPrimaryEnum.greenTosca,
									border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
								}}
								type='primary'
								icon={<DownloadOutlined />}>
								Download
							</Button>
						</Space>
					</Space>
				</Row>
				<Table
					loading={isLoading}
					bordered
					columns={columnsTable}
					dataSource={dataTable}
					scroll={{ x: 2500 }}
				/>
			</div>
		</Content>
	);
};

export default PriceAndRanking;
