import {
	FileAddOutlined,
	InboxOutlined,
	UploadOutlined,
} from '@ant-design/icons';
import { Button, Form, Layout, Typography, Upload } from 'antd';
import React from 'react';
import { ColorBaseEnum, ColorPrimaryEnum } from '../styles/Colors';

const { Content } = Layout;
const { Title } = Typography;

const UploadBudget = (props) => {
	const [form] = Form.useForm();
	const normFile = (e) => {
		console.log('Upload event:', e);
		if (Array.isArray(e)) {
			return e;
		}
		return e && e.fileList;
	};

	return (
		<Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
			<Title level={4}>Upload Budget</Title>
			<div className='site-layout-background' style={{ padding: 24 }}>
				<Form scrollToFirstError form={form}>
					<Form.Item>
						<Form.Item
							name='uploadFileRFQ'
							valuePropName='fileList'
							getValueFromEvent={normFile}
							noStyle>
							<Upload.Dragger name='files' action='/upload.do'>
								<p className='ant-upload-drag-icon'>
									<InboxOutlined />
								</p>
								<p className='ant-upload-text'>
									Click or drag file to this area to upload
								</p>
								<p className='ant-upload-hint'>
									Support for a single or bulk upload.
								</p>
							</Upload.Dragger>
						</Form.Item>
					</Form.Item>

					<Form.Item>
						<Button
							htmlType='submit'
							block
							style={{
								backgroundColor: ColorBaseEnum.white,
								color: ColorPrimaryEnum.redVelvet,
								border: `1px solid ${ColorPrimaryEnum.redVelvet}`,
							}}
							type='primary'
							icon={<UploadOutlined />}>
							Upload File
						</Button>
					</Form.Item>
				</Form>

				<Button
					block
					style={{
						backgroundColor: ColorBaseEnum.white,
						color: ColorPrimaryEnum.greenTosca,
						border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
					}}
					type='primary'
					icon={<FileAddOutlined />}>
					Download Template
				</Button>
			</div>
		</Content>
	);
};

export default UploadBudget;
