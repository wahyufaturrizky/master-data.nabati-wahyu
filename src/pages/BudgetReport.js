import {
	EyeOutlined,
	FileAddOutlined,
	InboxOutlined,
	UploadOutlined,
} from '@ant-design/icons';
import {
	Button,
	Col,
	DatePicker,
	Form,
	Input,
	Layout,
	Modal,
	Row,
	Select,
	Space,
	Table,
	Tabs,
	Typography,
	Upload,
} from 'antd';
import React, { useState } from 'react';
import { ColorBaseEnum, ColorPrimaryEnum } from '../styles/Colors';
import { MarginEnum } from '../styles/Spacer';

const { Content } = Layout;
const { TabPane } = Tabs;
const { Option } = Select;
const { Title, Text } = Typography;

const formItemLayout = {
	labelCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 8,
		},
	},
	wrapperCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 16,
		},
	},
};

const BudgetReport = (props) => {
	const [modal, contextHolder] = Modal.useModal();
	const [form] = Form.useForm();
	const [isModalActionVisible, setIsModalActionVisible] = useState({
		dataRow: null,
		typeAction: '',
		isShowModalAction: false,
	});
	const [isLoading, setIsLoading] = useState(false);
	const { isShowModalAction, typeAction, dataRow } = isModalActionVisible;

	const normFile = (e: any) => {
		console.log('Upload event:', e);
		if (Array.isArray(e)) {
			return e;
		}
		return e && e.fileList;
	};

	const config = {
		title: 'Success Message!',
		content: <p>Success create supplier!</p>,
		onOk: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			window.location.reload();
		},
		onCancel: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
		},
	};

	const updateSupllier = {
		title: 'Success Message!',
		content: <p>Success update supplier!</p>,
		onOk: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			window.location.reload();
		},
		onCancel: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
		},
	};

	const onFinish = async (values) => {
		setIsLoading(true);
		modal.confirm('add');
		setIsLoading(false);
	};

	const onEditSupplier = async (values) => {
		setIsLoading(true);
		modal.confirm('udpate');
		setIsLoading(false);
	};

	const onDeleteSupplier = async (values) => {
		window.alert('Success delete supplier');
		setIsModalActionVisible({
			...isModalActionVisible,
			isShowModalAction: false,
		});
		window.location.reload();
	};

	const dataTable = [
		{
			key: 1,
			company: 'KSNI',
			period: '20-08-2021',
			materialDesc: 'ROL AHH RCO 5.5G (D2)',
			budget: '100.000.000',
			createOn: '20-08-2021',
			createBy: '20-08-2021',
		},
	];

	const columnsTable = [
		{
			title: 'No',
			dataIndex: 'key',
			key: 'key',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Company',
			dataIndex: 'company',
			key: 'company',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Period',
			dataIndex: 'period',
			key: 'period',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Description',
			dataIndex: 'materialDesc',
			key: 'materialDesc',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Budget',
			dataIndex: 'budget',
			key: 'budget',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Create On',
			dataIndex: 'createOn',
			key: 'createOn',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Create By',
			dataIndex: 'createBy',
			key: 'createBy',
			render: (text) => <p>{text}</p>,
		},
	];

	return (
		<Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
			<Title level={4}>Budget Report</Title>
			<div className='site-layout-background' style={{ padding: 24 }}>
				<Row
					style={{
						marginBottom: MarginEnum['2x'],
					}}>
					<Space>
						<Space direction='vertical'>
							<Text>Company</Text>
							<Select
								allowClear
								showSearch
								style={{ width: 200 }}
								placeholder='Select'
								optionFilterProp='children'
								onChange={(e) => console.log(`onChange company ${e}`)}
								onFocus={(e) => console.log(`onFocus company ${e}`)}
								onBlur={(e) => console.log(`onBlur company ${e}`)}
								onSearch={(e) => console.log(`onSearch company ${e}`)}
								filterOption={(input, option) =>
									option.children.toLowerCase().indexOf(input.toLowerCase()) >=
									0
								}>
								<Option value='jack'>Jack</Option>
								<Option value='lucy'>Lucy</Option>
								<Option value='tom'>Tom</Option>
							</Select>
						</Space>
						<Space direction='vertical'>
							<Text>Material Group</Text>
							<Select
								allowClear
								showSearch
								style={{ width: 200 }}
								placeholder='Select'
								optionFilterProp='children'
								onChange={(e) => console.log(`onChange Material Group ${e}`)}
								onFocus={(e) => console.log(`onFocus Material Group ${e}`)}
								onBlur={(e) => console.log(`onBlur Material Group ${e}`)}
								onSearch={(e) => console.log(`onSearch Material Group ${e}`)}
								filterOption={(input, option) =>
									option.children.toLowerCase().indexOf(input.toLowerCase()) >=
									0
								}>
								<Option value='jack'>Jack</Option>
								<Option value='lucy'>Lucy</Option>
								<Option value='tom'>Tom</Option>
							</Select>
						</Space>
						<Space direction='vertical'>
							<Text>Valid From</Text>
							<DatePicker />
						</Space>
						<Space direction='vertical'>
							<Text>Valid To</Text>
							<DatePicker />
						</Space>
						<Space direction='vertical'>
							<Text style={{ color: ColorBaseEnum.white }}>text</Text>
							<Button
								style={{
									backgroundColor: ColorPrimaryEnum.redVelvet,
									border: `1px solid ${ColorPrimaryEnum.redVelvet}`,
								}}
								type='primary'
								icon={<EyeOutlined />}>
								View
							</Button>
						</Space>
						<Space direction='vertical'>
							<Text style={{ color: ColorBaseEnum.white }}>text</Text>
							<Button
								style={{
									backgroundColor: ColorPrimaryEnum.greenTosca,
									border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
								}}
								type='primary'
								icon={<EyeOutlined />}>
								Download
							</Button>
						</Space>
					</Space>
				</Row>
				<Table
					loading={isLoading}
					bordered
					columns={columnsTable}
					dataSource={dataTable}
					scroll={{ x: 1800 }}
				/>
			</div>

			<Modal
				title={typeAction}
				forceRender={false}
				visible={isShowModalAction}
				width={typeAction === 'ADD' || typeAction === 'EDIT' ? 1000 : undefined}
				afterClose={() => form.resetFields()}
				onOk={() => {
					if (typeAction === 'DELETE') {
						onDeleteSupplier(dataRow.guid);
					} else {
						form.resetFields();
						setIsModalActionVisible({
							...isModalActionVisible,
							isShowModalAction: false,
						});
					}
				}}
				onCancel={() => {
					form.resetFields();
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
				}}>
				{typeAction === 'VIEW' ? (
					dataRow &&
					Object.keys(dataRow).map((data, index) => {
						if (
							data === 'key' ||
							data === 'guid' ||
							data === 'updated_at' ||
							data === 'created_at'
						) {
							return null;
						}
						return (
							<p key={index} style={{ fontWeight: 'bold', color: '#595959' }}>
								{data.replace('_', ' ')} :{' '}
								<span style={{ fontWeight: 'normal' }}>{dataRow[data]}</span>
							</p>
						);
					})
				) : typeAction === 'EDIT' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='addSupplier'
						onFinish={onEditSupplier}
						fields={[
							{
								name: ['company'],
								value: dataRow.company,
							},
							{
								name: ['vendor'],
								value: dataRow.vendor,
							},
							{
								name: ['vendorDesc'],
								value: dataRow.vendorDesc,
							},
							{
								name: ['materialNo'],
								value: dataRow.materialNo,
							},
							{
								name: ['materialDesc'],
								value: dataRow.materialDesc,
							},
							{
								name: ['ratio'],
								value: dataRow.ratio,
							},
							{
								name: ['status'],
								value: dataRow.status,
							},
						]}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='company'
									label='Company'
									rules={[
										{
											required: true,
											message: 'Please input company!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange company ${e}`)}
										onFocus={(e) => console.log(`onFocus company ${e}`)}
										onBlur={(e) => console.log(`onBlur company ${e}`)}
										onSearch={(e) => console.log(`onSearch company ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='vendor'
									label='Vendor'
									rules={[
										{
											required: true,
											message: 'Please input vendor!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange vendor ${e}`)}
										onFocus={(e) => console.log(`onFocus vendor ${e}`)}
										onBlur={(e) => console.log(`onBlur vendor ${e}`)}
										onSearch={(e) => console.log(`onSearch vendor ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='materialNo'
									label='Material'
									rules={[
										{
											required: true,
											message: 'Please input material!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange material ${e}`)}
										onFocus={(e) => console.log(`onFocus material ${e}`)}
										onBlur={(e) => console.log(`onBlur material ${e}`)}
										onSearch={(e) => console.log(`onSearch material ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='ratio'
									label='Ratio'
									rules={[
										{
											required: true,
											message: 'Please input ratio!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>

						<Form.Item hidden name='guid' label='guid'>
							<Input />
						</Form.Item>
						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Save
								</Button>
							</Form.Item>
							{contextHolder}
						</div>
					</Form>
				) : typeAction === 'ADD' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='addSupplier'
						onFinish={onFinish}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='company'
									label='Company'
									rules={[
										{
											required: true,
											message: 'Please input company!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange company ${e}`)}
										onFocus={(e) => console.log(`onFocus company ${e}`)}
										onBlur={(e) => console.log(`onBlur company ${e}`)}
										onSearch={(e) => console.log(`onSearch company ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='vendor'
									label='Vendor'
									rules={[
										{
											required: true,
											message: 'Please input vendor!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange vendor ${e}`)}
										onFocus={(e) => console.log(`onFocus vendor ${e}`)}
										onBlur={(e) => console.log(`onBlur vendor ${e}`)}
										onSearch={(e) => console.log(`onSearch vendor ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='materialNo'
									label='Material'
									rules={[
										{
											required: true,
											message: 'Please input material!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange material ${e}`)}
										onFocus={(e) => console.log(`onFocus material ${e}`)}
										onBlur={(e) => console.log(`onBlur material ${e}`)}
										onSearch={(e) => console.log(`onSearch material ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='ratio'
									label='Ratio'
									rules={[
										{
											required: true,
											message: 'Please input ratio!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>

						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Create
								</Button>
							</Form.Item>
							{contextHolder}
						</div>
					</Form>
				) : typeAction === 'UPLOAD' ? (
					<>
						<Form.Item>
							<Form.Item
								name='uploadFileMdDesign'
								valuePropName='fileList'
								getValueFromEvent={normFile}
								noStyle>
								<Upload.Dragger name='files' action='/upload.do'>
									<p className='ant-upload-drag-icon'>
										<InboxOutlined />
									</p>
									<p className='ant-upload-text'>
										Click or drag file to this area to upload
									</p>
									<p className='ant-upload-hint'>
										Support for a single or bulk upload.
									</p>
								</Upload.Dragger>
							</Form.Item>
						</Form.Item>

						<Button
							block
							style={{
								backgroundColor: ColorBaseEnum.white,
								color: ColorPrimaryEnum.redVelvet,
								border: `1px solid ${ColorPrimaryEnum.redVelvet}`,
								marginBottom: MarginEnum['2x'],
							}}
							type='primary'
							icon={<UploadOutlined />}>
							Upload File
						</Button>
						<Button
							block
							style={{
								backgroundColor: ColorBaseEnum.white,
								color: ColorPrimaryEnum.greenTosca,
								border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
							}}
							type='primary'
							icon={<FileAddOutlined />}>
							Download Template
						</Button>
					</>
				) : (
					<p>Apakah anda ingin menghapus data ini ?</p>
				)}
			</Modal>
		</Content>
	);
};

export default BudgetReport;
