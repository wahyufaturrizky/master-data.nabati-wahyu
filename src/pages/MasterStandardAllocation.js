import {
	DeleteOutlined,
	EditOutlined,
	FileAddOutlined,
} from '@ant-design/icons';
import {
	Button,
	Col,
	Form,
	Input,
	Layout,
	Modal,
	Row,
	Select,
	Space,
	Table,
	Tabs,
	Typography,
} from 'antd';
import React, { useState } from 'react';
import { ColorPrimaryEnum } from '../styles/Colors';

const { Content } = Layout;
const { Option } = Select;
const { Title } = Typography;
const { TabPane } = Tabs;

const formItemLayout = {
	labelCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 8,
		},
	},
	wrapperCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 16,
		},
	},
};

const MasterStandardAllocation = (props) => {
	const [modal, contextHolder] = Modal.useModal();
	const [form] = Form.useForm();
	const [isModalActionVisible, setIsModalActionVisible] = useState({
		dataRow: null,
		typeAction: '',
		isShowModalAction: false,
	});
	const [isLoading, setIsLoading] = useState(false);
	const { isShowModalAction, typeAction, dataRow } = isModalActionVisible;

	const config = {
		title: 'Success Message!',
		content: <p>Success create supplier!</p>,
		onOk: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			window.location.reload();
		},
		onCancel: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
		},
	};

	const updateSupllier = {
		title: 'Success Message!',
		content: <p>Success update supplier!</p>,
		onOk: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			window.location.reload();
		},
		onCancel: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
		},
	};

	const onFinish = async (values) => {
		setIsLoading(true);
		modal.confirm('add');
		setIsLoading(false);
	};

	const onEditSupplier = async (values) => {
		setIsLoading(true);
		modal.confirm('udpate');
		setIsLoading(false);
	};

	const onDeleteSupplier = async (values) => {
		window.alert('Success delete supplier');
		setIsModalActionVisible({
			...isModalActionVisible,
			isShowModalAction: false,
		});
		window.location.reload();
	};

	const dataTableJumlahVendor = [
		{
			key: 1,
			materialGroup: 'Roll Film',
			qtyMin: '10',
			qtyMax: '10',
			jumlahMaxVendor: '10',
		},
	];

	const dataTableRatio = [
		{
			key: '1',
		},
		{
			key: '2',
		},
		{
			key: '3',
		},
		{
			key: '4',
		},
		{
			key: '5',
		},
		{
			key: '6',
		},
		{
			key: '7',
		},
		{
			key: '8',
		},
		{
			key: '9',
		},
		{
			key: '10',
		},
		{
			key: '11',
		},
		{
			key: '12',
		},
	];

	const columnsTableRatio = [
		{
			title: 'JUMLAH VENDOR',
			dataIndex: 'key',
			key: 'key',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'RANKING',
			children: [
				{
					title: '1',
					dataIndex: 'one',
					key: 'one',
					render: (text) => <p>{text}</p>,
				},
				{
					title: '2',
					dataIndex: 'one',
					key: 'one',
					render: (text) => <p>{text}</p>,
				},
				{
					title: '3',
					dataIndex: 'one',
					key: 'one',
					render: (text) => <p>{text}</p>,
				},
				{
					title: '4',
					dataIndex: 'one',
					key: 'one',
					render: (text) => <p>{text}</p>,
				},
				{
					title: '5',
					dataIndex: 'one',
					key: 'one',
					render: (text) => <p>{text}</p>,
				},
				{
					title: '6',
					dataIndex: 'one',
					key: 'one',
					render: (text) => <p>{text}</p>,
				},
				{
					title: '7',
					dataIndex: 'one',
					key: 'one',
					render: (text) => <p>{text}</p>,
				},
				{
					title: '8',
					dataIndex: 'one',
					key: 'one',
					render: (text) => <p>{text}</p>,
				},
				{
					title: '9',
					dataIndex: 'one',
					key: 'one',
					render: (text) => <p>{text}</p>,
				},
				{
					title: '10',
					dataIndex: 'one',
					key: 'one',
					render: (text) => <p>{text}</p>,
				},
				{
					title: '11',
					dataIndex: 'one',
					key: 'one',
					render: (text) => <p>{text}</p>,
				},
				{
					title: '12',
					dataIndex: 'one',
					key: 'one',
					render: (text) => <p>{text}</p>,
				},
			],
		},
		{
			title: 'Action',
			key: 'action',
			render: (text, record) => {
				return (
					<Space size='middle'>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'EDIT',
								})
							}
							type='default'
							icon={<EditOutlined />}>
							Edit
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									...isModalActionVisible,
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'DELETE',
								})
							}
							type='danger'
							icon={<DeleteOutlined />}>
							Delete
						</Button>
					</Space>
				);
			},
		},
	];

	const columnsTableJumlahVendor = [
		{
			title: 'No',
			dataIndex: 'key',
			key: 'key',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Group',
			dataIndex: 'materialGroup',
			key: 'materialGroup',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Qty Min',
			dataIndex: 'qtyMin',
			key: 'qtyMin',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Qty Max',
			dataIndex: 'qtyMax',
			key: 'qtyMax',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Jumlah Max Vendor',
			dataIndex: 'jumlahMaxVendor',
			key: 'jumlahMaxVendor',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Action',
			key: 'action',
			render: (text, record) => {
				return (
					<Space size='middle'>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'EDIT',
								})
							}
							type='default'
							icon={<EditOutlined />}>
							Edit
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									...isModalActionVisible,
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'DELETE',
								})
							}
							type='danger'
							icon={<DeleteOutlined />}>
							Delete
						</Button>
					</Space>
				);
			},
		},
	];

	return (
		<Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
			<Title level={4}>Standard Allocation</Title>
			<div className='site-layout-background' style={{ padding: 24 }}>
				<Row
					justify='end'
					style={{
						marginBottom: 12,
					}}>
					<Button
						onClick={() =>
							setIsModalActionVisible({
								...isModalActionVisible,
								typeAction: 'ADD',
								isShowModalAction: true,
							})
						}
						style={{
							backgroundColor: ColorPrimaryEnum.greenTosca,
							border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
						}}
						type='primary'
						icon={<FileAddOutlined />}>
						Add
					</Button>
				</Row>
				<Tabs defaultActiveKey='1'>
					<TabPane tab='Jumlah Vendor' key='1'>
						<Table
							loading={isLoading}
							bordered
							columns={columnsTableJumlahVendor}
							dataSource={dataTableJumlahVendor}
						/>
					</TabPane>
					<TabPane tab='Ratio Vendor' key='2'>
						<Table
							loading={isLoading}
							bordered
							columns={columnsTableRatio}
							dataSource={dataTableRatio}
						/>
					</TabPane>
				</Tabs>
			</div>

			<Modal
				title={typeAction}
				forceRender={false}
				visible={isShowModalAction}
				width={typeAction === 'ADD' || typeAction === 'EDIT' ? 1000 : undefined}
				afterClose={() => form.resetFields()}
				onOk={() => {
					if (typeAction === 'DELETE') {
						onDeleteSupplier(dataRow.guid);
					} else {
						form.resetFields();
						setIsModalActionVisible({
							...isModalActionVisible,
							isShowModalAction: false,
						});
					}
				}}
				onCancel={() => {
					form.resetFields();
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
				}}>
				{typeAction === 'VIEW' ? (
					dataRow &&
					Object.keys(dataRow).map((data, index) => {
						if (
							data === 'key' ||
							data === 'guid' ||
							data === 'updated_at' ||
							data === 'created_at'
						) {
							return null;
						}
						return (
							<p key={index} style={{ fontWeight: 'bold', color: '#595959' }}>
								{data.replace('_', ' ')} :{' '}
								<span style={{ fontWeight: 'normal' }}>{dataRow[data]}</span>
							</p>
						);
					})
				) : typeAction === 'EDIT' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='addSupplier'
						onFinish={onEditSupplier}
						fields={[
							{
								name: ['materialGroup'],
								value: dataRow.materialGroup,
							},
							{
								name: ['materialGroupDesc'],
								value: dataRow.materialGroupDesc,
							},
						]}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='materialGroup'
									label='Material Group'
									rules={[
										{
											required: true,
											message: 'Please input material group!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='materialGroupDesc'
									label='Material Group Desc.'
									rules={[
										{
											required: true,
											message: 'Please input material group desc. !',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>

						<Form.Item hidden name='guid' label='guid'>
							<Input />
						</Form.Item>
						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Save
								</Button>
							</Form.Item>
							{contextHolder}
						</div>
					</Form>
				) : typeAction === 'ADD' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='addSupplier'
						onFinish={onFinish}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='materialGroup'
									label='Material Group'
									rules={[
										{
											required: true,
											message: 'Please input material group!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='materialGroupDesc'
									label='Material Group Desc.'
									rules={[
										{
											required: true,
											message: 'Please input material group desc. !',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>

						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Create
								</Button>
							</Form.Item>
							{contextHolder}
						</div>
					</Form>
				) : (
					<p>Apakah anda ingin menghapus data ini ?</p>
				)}
			</Modal>
		</Content>
	);
};

export default MasterStandardAllocation;
