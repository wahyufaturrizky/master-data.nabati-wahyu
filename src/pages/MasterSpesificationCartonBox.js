import {
	ArrowLeftOutlined,
	DeleteOutlined,
	EditOutlined,
	FileAddOutlined,
	UnorderedListOutlined,
} from '@ant-design/icons';
import {
	Button,
	Col,
	Form,
	Input,
	Layout,
	Modal,
	Radio,
	Row,
	Select,
	Space,
	Table,
	Tabs,
	Typography,
} from 'antd';
import moment from 'moment';
import React, { useCallback, useEffect, useState } from 'react';
import {
	deleteDataListMasterCartoonBoxFluteByVendor,
	deleteDataListMasterCartoonBoxFluteGeneral,
	deleteDataListMasterCartoonBoxLayerSekat,
	deleteDataListMasterSpecCartonBox,
	getDataListCharacteritic,
	getDataListMasterCartoonBoxFluteGeneral,
	getDataListMasterCartoonBoxFluteVendor,
	getDataListMasterCartoonBoxLayerSekat,
	getDataListMasterSpecCartoonBox,
	getDataListVendor,
	postDataListMasterCartoonBoxFluteGeneral,
	postDataListMasterFluteByVendor,
	postDataListMasterLayerSekat,
	postDataListMasterSpecCartonBox,
	postLogin,
	putDataListMasterCartoonBoxFluteByVendor,
	putDataListMasterCartoonBoxFluteGeneral,
	putDataListMasterCartoonBoxLayerSekat,
	putDataListMasterSpecCartonBox,
} from '../services/retrieveData';
import { ColorPrimaryEnum } from '../styles/Colors';

const { Content } = Layout;
const { Title } = Typography;
const { Option } = Select;
const { TabPane } = Tabs;

const formItemLayout = {
	labelCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 8,
		},
	},
	wrapperCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 16,
		},
	},
};

const MasterSpesificationCartonBox = (props) => {
	const [modal, contextHolder] = Modal.useModal();
	const [form] = Form.useForm();
	const [dataListMasterSpec, setDataListMasterSpec] = useState([]);
	const [dataListFluteGeneral, setDataListFluteGeneral] = useState([]);
	const [dataListFluteVendor, setDataListFluteVendor] = useState([]);
	const [dataListLayerSekat, setDataListLayerSekat] = useState([]);
	const [dataCharacter, setDataCharacter] = useState([]);
	const [dataListVendor, setDataListVendor] = useState([]);
	const [dataGroup, setDataGroup] = useState(null);
	const [dataTypeLayerSekat, setDataTypeLayerSekat] = useState(null);
	const [isModalActionVisible, setIsModalActionVisible] = useState({
		dataRow: null,
		typeAction: '',
		isShowModalAction: false,
	});
	const [isLoading, setIsLoading] = useState(false);
	const { isShowModalAction, typeAction, dataRow } = isModalActionVisible;

	const fetchDataListFluteGeneral = useCallback(async ({ keyword }) => {
		setIsLoading(true);
		let data = {
			material_group: props.location.state.material_group,
			search: keyword || '',
		};
		const res = await getDataListMasterCartoonBoxFluteGeneral(data).catch(
			(err) => {
				setIsLoading(false);
				modal.error({
					title: 'Failed Message!',
					content: (
						<p>{err.response.data.message || 'Internal server error'}</p>
					),
					onOk: () => {},
					onCancel: () => {},
				});
			}
		);

		if (res && res.status === 200) {
			setIsLoading(false);
			setDataListFluteGeneral(res.data.items.data);
		}
	}, []);

	const fetchDataListMasterSpec = useCallback(async ({ keyword }) => {
		setIsLoading(true);
		let data = {
			material_group: props.location.state.material_group,
			search: keyword || '',
		};
		const res = await getDataListMasterSpecCartoonBox(data).catch((err) => {
			setIsLoading(false);
			modal.error({
				title: 'Failed Message!',
				content: <p>{err.response.data.message || 'Internal server error'}</p>,
				onOk: () => {},
				onCancel: () => {},
			});
		});

		if (res && res.status === 200) {
			setIsLoading(false);
			setDataListMasterSpec(res.data.items.data);
			setDataGroup(res.data.groups);
		}
	}, []);

	const fetchDataListFluteVendor = useCallback(async ({ keyword }) => {
		setIsLoading(true);
		let data = {
			material_group: props.location.state.material_group,
			search: keyword || '',
		};
		const res = await getDataListMasterCartoonBoxFluteVendor(data).catch(
			(err) => {
				setIsLoading(false);
				modal.error({
					title: 'Failed Message!',
					content: (
						<p>{err.response.data.message || 'Internal server error'}</p>
					),
					onOk: () => {},
					onCancel: () => {},
				});
			}
		);

		if (res && res.status === 200) {
			setIsLoading(false);
			setDataListFluteVendor(res.data.items.data);
		}
	}, []);

	const fetchDataListLayerSekat = useCallback(async ({ keyword }) => {
		setIsLoading(true);
		let data = {
			material_group: props.location.state.material_group,
			search: keyword || '',
		};
		const res = await getDataListMasterCartoonBoxLayerSekat(data).catch(
			(err) => {
				setIsLoading(false);
				modal.error({
					title: 'Failed Message!',
					content: (
						<p>{err.response.data.message || 'Internal server error'}</p>
					),
					onOk: () => {},
					onCancel: () => {},
				});
			}
		);

		if (res && res.status === 200) {
			setIsLoading(false);
			setDataListLayerSekat(res.data.items.data);
			setDataTypeLayerSekat(res.data.types);
		}
	}, []);

	useEffect(() => {
		const fetchDataListVendor = async () => {
			setIsLoading(true);
			const res = await getDataListVendor().catch((err) => {
				setIsLoading(false);
				modal.error({
					title: 'Failed Message!',
					content: (
						<p>{err.response.data.message || 'Internal server error'}</p>
					),
					onOk: () => {},
					onCancel: () => {},
				});
			});

			if (res && res.status === 200) {
				setIsLoading(false);
				setDataListVendor(res.data.items);
			}
		};

		const fetchDataLisCharacteritic = async () => {
			setIsLoading(true);
			const data = {
				material_group_id: props.location.state.material_group,
				search: '',
			};

			const res = await getDataListCharacteritic(data).catch((err) => {
				setIsLoading(false);
				if (err?.response?.data?.message === 'Unauthenticated.') {
					modal.warning({
						title: 'Warning Message!',
						content: (
							<p>Your session has expired, click OK to renew your session</p>
						),
						onOk: async () => {
							setIsLoading(true);
							const dataLogin = await localStorage.getItem('dataUser');
							const dataLoginParse = JSON.parse(dataLogin);
							let data = new FormData();

							data.append('username', dataLoginParse.username || 'user_demo');
							data.append('password', dataLoginParse.password || '12345');
							const res = await postLogin(data).catch((err) => {
								window.alert('Failed login');
								setIsLoading(false);
							});

							if (res && res.status === 200) {
								if (res.data === '') {
									setIsLoading(false);
									modal.error({
										title: 'Failed login!',
										content: (
											<p>Username/password is not correct, please try again</p>
										),
										onOk: () => {},
									});
								} else {
									localStorage.setItem('dataUser', JSON.stringify(res.data));
									window.location.reload();
									setIsLoading(false);
								}
							}
						},
					});
				} else {
					modal.error({
						title: 'Failed Message!',
						content: (
							<p>{err?.response?.data?.message || 'Internal server error'}</p>
						),
						onOk: () => {},
					});
				}
			});

			if (res && res.status === 200) {
				setIsLoading(false);
				setDataCharacter(res.data.items);
			}
		};

		fetchDataListMasterSpec({});
		fetchDataListVendor();
		fetchDataListFluteGeneral({});
		fetchDataListFluteVendor({});
		fetchDataListLayerSekat({});
		fetchDataLisCharacteritic();
	}, [
		fetchDataListFluteGeneral,
		fetchDataListMasterSpec,
		fetchDataListFluteVendor,
		fetchDataListLayerSekat,
	]);

	const onFinish = async (values) => {
		setIsLoading(true);

		let data = {
			characteristic_id: values.characteristic_id,
			specification_id: props.location.state.id,
			group_id: parseInt(values.group_id),
			characteristic_description: values.characteristic_description,
			range: values.range,
		};

		const res = await postDataListMasterSpecCartonBox(data).catch((err) => {
			setIsLoading(false);
			modal.error({
				title: 'Failed Message!',
				content: <p>{err.response.data.message || 'Internal server error'}</p>,
				onOk: () => {
					form.resetFields();
				},
			});
		});

		if (res && res.status === 200) {
			setIsLoading(false);
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					form.resetFields();
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
					fetchDataListMasterSpec({});
				},
			});
		}
	};

	const onFinishFluteGeneral = async (values) => {
		setIsLoading(true);

		let data = {
			flute: values.flute,
			specification_id: props.location.state.id,
			p_tol: values.p_tol,
			l_tol: values.l_tol,
			take_up: values.take_up,
			kuping: values.kuping,
		};

		const res = await postDataListMasterCartoonBoxFluteGeneral(data).catch(
			(err) => {
				setIsLoading(false);
				modal.error({
					title: 'Failed Message!',
					content: (
						<p>{err.response.data.message || 'Internal server error'}</p>
					),
					onOk: () => {
						form.resetFields();
					},
				});
			}
		);

		if (res && res.status === 200) {
			setIsLoading(false);
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					form.resetFields();
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
					fetchDataListFluteGeneral({});
				},
			});
		}
	};

	const onFinishFluteByVendor = async (values) => {
		setIsLoading(true);

		let data = {
			vendor_id: values.vendor_id,
			specification_id: props.location.state.id,
		};

		const res = await postDataListMasterFluteByVendor(data).catch((err) => {
			setIsLoading(false);
			modal.error({
				title: 'Failed Message!',
				content: <p>{err.response.data.message || 'Internal server error'}</p>,
				onOk: () => {
					form.resetFields();
				},
			});
		});

		if (res && res.status === 200) {
			setIsLoading(false);
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					form.resetFields();
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
					fetchDataListFluteVendor({});
				},
			});
		}
	};

	const onFinishLayerSekat = async (values) => {
		setIsLoading(true);

		let data = {
			vendor_id: values.vendor_id,
			type: values.type,
			specification_id: props.location.state.id,
		};

		const res = await postDataListMasterLayerSekat(data).catch((err) => {
			setIsLoading(false);
			modal.error({
				title: 'Failed Message!',
				content: <p>{err.response.data.message || 'Internal server error'}</p>,
				onOk: () => {
					form.resetFields();
				},
			});
		});

		if (res && res.status === 200) {
			setIsLoading(false);
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					form.resetFields();
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
					fetchDataListLayerSekat({});
				},
			});
		}
	};

	const onEdit = async (values) => {
		setIsLoading(true);

		let data = {
			characteristic_id: values.characteristic_id,
			specification_id: props.location.state.id,
			group_id: parseInt(values.group_id),
			characteristic_description: values.characteristic_description,
			range: values.range,
		};

		const res = await putDataListMasterSpecCartonBox(dataRow.id, data).catch(
			(err) => {
				setIsLoading(false);
				modal.error({
					title: 'Failed Message!',
					content: (
						<p>{err.response.data.message || 'Internal server error'}</p>
					),
				});
			}
		);

		if (res && res.status === 200) {
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
					fetchDataListMasterSpec({});
				},
			});
		}
	};

	const onEditLayerSekat = async (values) => {
		setIsLoading(true);

		let data = {
			vendor_id: values.vendor_id,
			type: values.type,
			specification_id: props.location.state.id,
		};

		const res = await putDataListMasterCartoonBoxLayerSekat(
			dataRow.id,
			data
		).catch((err) => {
			setIsLoading(false);
			modal.error({
				title: 'Failed Message!',
				content: <p>{err.response.data.message || 'Internal server error'}</p>,
			});
		});

		if (res && res.status === 200) {
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
					fetchDataListLayerSekat({});
				},
			});
		}
	};

	const onEditFluteGeneral = async (values) => {
		setIsLoading(true);

		let data = {
			flute: values.flute,
			specification_id: props.location.state.id,
			p_tol: values.p_tol,
			l_tol: values.l_tol,
			take_up: values.take_up,
			kuping: values.kuping,
		};

		const res = await putDataListMasterCartoonBoxFluteGeneral(
			dataRow.id,
			data
		).catch((err) => {
			setIsLoading(false);
			modal.error({
				title: 'Failed Message!',
				content: <p>{err.response.data.message || 'Internal server error'}</p>,
			});
		});

		if (res && res.status === 200) {
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
					fetchDataListFluteGeneral({});
				},
			});
		}
	};

	const onEditFluteByVendor = async (values) => {
		setIsLoading(true);

		let data = {
			vendor_id: values.vendor_id,
			specification_id: props.location.state.id,
		};

		const res = await putDataListMasterCartoonBoxFluteByVendor(
			dataRow.id,
			data
		).catch((err) => {
			setIsLoading(false);
			modal.error({
				title: 'Failed Message!',
				content: <p>{err.response.data.message || 'Internal server error'}</p>,
			});
		});

		if (res && res.status === 200) {
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
					fetchDataListFluteVendor({});
				},
			});
		}
	};

	const onDelete = async (values) => {
		setIsLoading(true);
		const res = await deleteDataListMasterSpecCartonBox(values).catch((err) => {
			setIsLoading(false);
			modal.error({
				title: 'Failed Message!',
				content: <p>{err.response.data.message || 'Internal server error'}</p>,
				onOk: () => {
					form.resetFields();
				},
			});
		});

		if (res && res.status === 200) {
			setIsLoading(false);
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					form.resetFields();
					fetchDataListMasterSpec({});
				},
			});
		}
	};

	const onDeleteFluteGeneral = async (values) => {
		setIsLoading(true);
		const res = await deleteDataListMasterCartoonBoxFluteGeneral(values).catch(
			(err) => {
				setIsLoading(false);
				modal.error({
					title: 'Failed Message!',
					content: (
						<p>{err.response.data.message || 'Internal server error'}</p>
					),
					onOk: () => {
						form.resetFields();
					},
				});
			}
		);

		if (res && res.status === 200) {
			setIsLoading(false);
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					form.resetFields();
					fetchDataListFluteGeneral({});
				},
			});
		}
	};

	const onDeleteFluteByVendor = async (values) => {
		setIsLoading(true);
		const res = await deleteDataListMasterCartoonBoxFluteByVendor(values).catch(
			(err) => {
				setIsLoading(false);
				modal.error({
					title: 'Failed Message!',
					content: (
						<p>{err.response.data.message || 'Internal server error'}</p>
					),
					onOk: () => {
						form.resetFields();
					},
				});
			}
		);

		if (res && res.status === 200) {
			setIsLoading(false);
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					form.resetFields();
					fetchDataListFluteVendor({});
				},
			});
		}
	};

	const onDeleteLayerSekat = async (values) => {
		setIsLoading(true);
		const res = await deleteDataListMasterCartoonBoxLayerSekat(values).catch(
			(err) => {
				setIsLoading(false);
				modal.error({
					title: 'Failed Message!',
					content: (
						<p>{err.response.data.message || 'Internal server error'}</p>
					),
					onOk: () => {
						form.resetFields();
					},
				});
			}
		);

		if (res && res.status === 200) {
			setIsLoading(false);
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			modal.success({
				title: 'Success Message!',
				content: <p>{res.data.success}</p>,
				onOk: () => {
					form.resetFields();
					fetchDataListLayerSekat({});
				},
			});
		}
	};

	const columnsTable = [
		{
			title: 'No',
			dataIndex: 'id',
			key: 'id',
			render: (text, rowData, index) => <p>{index + 1}</p>,
		},
		{
			title: 'uuid',
			dataIndex: 'uuid',
			key: 'uuid',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'characteristic ID',
			dataIndex: 'characteristic_id',
			key: 'characteristic_id',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Characteristic',
			dataIndex: 'characteristic',
			key: 'characteristic',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Specification ID',
			dataIndex: 'specification_id',
			key: 'specification_id',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Group',
			dataIndex: 'material_group',
			key: 'material_group',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Group Desc',
			dataIndex: 'material_group_description',
			key: 'material_group_description',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Group ID',
			dataIndex: 'group_id',
			key: 'group_id',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Group Name',
			dataIndex: 'group_name',
			key: 'group_name',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Characteristic Desc',
			dataIndex: 'characteristic_description',
			key: 'characteristic_description',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Range',
			dataIndex: 'range',
			key: 'range',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'State',
			dataIndex: 'state',
			key: 'state',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Type',
			dataIndex: 'type',
			key: 'type',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Created',
			dataIndex: 'created_at',
			key: 'created_at',
			render: (text) => <p>{moment(text).format('YYYY-MM-DD')}</p>,
		},
		{
			title: 'Updated',
			dataIndex: 'updated_at',
			key: 'updated_at',
			render: (text) => <p>{moment(text).format('YYYY-MM-DD')}</p>,
		},
		{
			title: 'Action',
			key: 'action',
			render: (text, record) => {
				return (
					<Space size='middle'>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'EDIT SPEC',
								})
							}
							type='default'
							icon={<EditOutlined />}>
							Edit
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									...isModalActionVisible,
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'DELETE SPEC',
								})
							}
							type='danger'
							icon={<DeleteOutlined />}>
							Delete
						</Button>
					</Space>
				);
			},
		},
	];

	const columnsTableFluteGeneral = [
		{
			title: 'No',
			dataIndex: 'id',
			key: 'id',
			render: (text, rowData, index) => <p>{index + 1}</p>,
		},
		{
			title: 'uuid',
			dataIndex: 'uuid',
			key: 'uuid',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Specification ID',
			dataIndex: 'specification_id',
			key: 'specification_id',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Flute Vendor',
			dataIndex: 'flute_vendor',
			key: 'flute_vendor',
			render: (text) => <p>{text || 'N/A'}</p>,
		},
		{
			title: 'Material Group',
			dataIndex: 'material_group',
			key: 'material_group',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Group Desc',
			dataIndex: 'material_group_description',
			key: 'material_group_description',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Flute',
			dataIndex: 'flute',
			key: 'flute',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'P Tol',
			dataIndex: 'p_tol',
			key: 'p_tol',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'L Tol',
			dataIndex: 'l_tol',
			key: 'l_tol',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Take Up',
			dataIndex: 'take_up',
			key: 'take_up',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Kuping',
			dataIndex: 'kuping',
			key: 'kuping',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'State',
			dataIndex: 'state',
			key: 'state',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Type',
			dataIndex: 'type',
			key: 'type',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Created',
			dataIndex: 'created_at',
			key: 'created_at',
			render: (text) => <p>{moment(text).format('YYYY-MM-DD')}</p>,
		},
		{
			title: 'Updated',
			dataIndex: 'updated_at',
			key: 'updated_at',
			render: (text) => <p>{moment(text).format('YYYY-MM-DD')}</p>,
		},
		{
			title: 'Action',
			key: 'action',
			render: (text, record) => {
				return (
					<Space size='middle'>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'EDIT FLUTE GENERAL',
								})
							}
							type='default'
							icon={<EditOutlined />}>
							Edit
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									...isModalActionVisible,
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'DELETE FLUTE GENERAL',
								})
							}
							type='danger'
							icon={<DeleteOutlined />}>
							Delete
						</Button>
					</Space>
				);
			},
		},
	];

	const columnsTableFluteByVendor = [
		{
			title: 'No',
			dataIndex: 'id',
			key: 'id',
			render: (text, rowData, index) => <p>{index + 1}</p>,
		},
		{
			title: 'uuid',
			dataIndex: 'uuid',
			key: 'uuid',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Specification ID',
			dataIndex: 'specification_id',
			key: 'specification_id',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Group',
			dataIndex: 'material_group',
			key: 'material_group',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Group Desc',
			dataIndex: 'material_group_description',
			key: 'material_group_description',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Vendor ID',
			dataIndex: 'vendor_id',
			key: 'vendor_id',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Vendor Name',
			dataIndex: 'vendor_name',
			key: 'vendor_name',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Vendor Code',
			dataIndex: 'vendor_code',
			key: 'vendor_code',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Created',
			dataIndex: 'created_at',
			key: 'created_at',
			render: (text) => <p>{moment(text).format('YYYY-MM-DD')}</p>,
		},
		{
			title: 'Updated',
			dataIndex: 'updated_at',
			key: 'updated_at',
			render: (text) => <p>{moment(text).format('YYYY-MM-DD')}</p>,
		},
		{
			title: 'Action',
			key: 'action',
			render: (text, record) => {
				return (
					<Space size='middle'>
						<Button
							onClick={() => {
								props.history.push(
									'/master-specification-cartoon-box-flute-vendor-item',
									record
								);
							}}
							type='primary'
							icon={<UnorderedListOutlined />}>
							List
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'EDIT FLUTE BY VENDOR',
								})
							}
							type='default'
							icon={<EditOutlined />}>
							Edit
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									...isModalActionVisible,
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'DELETE FLUTE BY VENDOR',
								})
							}
							type='danger'
							icon={<DeleteOutlined />}>
							Delete
						</Button>
					</Space>
				);
			},
		},
	];

	const columnsTableLayerSekat = [
		{
			title: 'No',
			dataIndex: 'id',
			key: 'id',
			render: (text, rowData, index) => <p>{index + 1}</p>,
		},
		{
			title: 'uuid',
			dataIndex: 'uuid',
			key: 'uuid',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Specification ID',
			dataIndex: 'specification_id',
			key: 'specification_id',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Group',
			dataIndex: 'material_group',
			key: 'material_group',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Group Desc',
			dataIndex: 'material_group_description',
			key: 'material_group_description',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Vendor ID',
			dataIndex: 'vendor_id',
			key: 'vendor_id',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Vendor Name',
			dataIndex: 'vendor_name',
			key: 'vendor_name',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Vendor Code',
			dataIndex: 'vendor_code',
			key: 'vendor_code',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Constanta ref',
			dataIndex: 'constanta_reference',
			key: 'constanta_reference',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Type',
			dataIndex: 'type',
			key: 'type',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Created',
			dataIndex: 'created_at',
			key: 'created_at',
			render: (text) => <p>{moment(text).format('YYYY-MM-DD')}</p>,
		},
		{
			title: 'Updated',
			dataIndex: 'updated_at',
			key: 'updated_at',
			render: (text) => <p>{moment(text).format('YYYY-MM-DD')}</p>,
		},
		{
			title: 'Action',
			key: 'action',
			render: (text, record) => {
				return (
					<Space size='middle'>
						<Button
							onClick={() => {
								props.history.push(
									'/master-specification-cartoon-box-layer-sekat-item',
									record
								);
							}}
							type='primary'
							icon={<UnorderedListOutlined />}>
							List
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'EDIT LAYER SEKAT',
								})
							}
							type='default'
							icon={<EditOutlined />}>
							Edit
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									...isModalActionVisible,
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'DELETE LAYER SEKAT',
								})
							}
							type='danger'
							icon={<DeleteOutlined />}>
							Delete
						</Button>
					</Space>
				);
			},
		},
	];

	const onSearchSpec = (data) => {
		fetchDataListMasterSpec({ keyword: data });
	};

	const onSearchFluteGeneral = (data) => {
		fetchDataListFluteGeneral({ keyword: data });
	};

	const onSearchFluteVendor = (data) => {
		fetchDataListFluteVendor({ keyword: data });
	};

	const onSearchLayerSekat = (data) => {
		fetchDataListLayerSekat({ keyword: data });
	};

	return (
		<Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
			<Title level={4}>Master Specification</Title>
			<div className='site-layout-background' style={{ padding: 24 }}>
				<Row justify='space-between'>
					<Title level={4}>
						{props?.location?.state?.material_group} -{' '}
						{props?.location?.state?.material_group_description}
					</Title>
					<Button
						onClick={() => props.history.push('/master-specification')}
						type='default'
						icon={<ArrowLeftOutlined />}>
						Back to list
					</Button>
				</Row>
				<Tabs defaultActiveKey='1'>
					<TabPane tab='Specification' key='1'>
						<Row
							justify='space-between'
							style={{
								marginBottom: 12,
							}}>
							<Button
								onClick={() =>
									setIsModalActionVisible({
										...isModalActionVisible,
										typeAction: 'ADD SPEC',
										isShowModalAction: true,
									})
								}
								style={{
									backgroundColor: ColorPrimaryEnum.greenTosca,
									border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
								}}
								type='primary'
								icon={<FileAddOutlined />}>
								Add
							</Button>
							<Input.Search
								onSearch={onSearchSpec}
								allowClear
								style={{ width: '40%' }}
							/>
						</Row>
						<Table
							loading={isLoading}
							bordered
							rowClassName={(record, index) => {
								if (index % 2 === 1) {
									return 'color-gray-2';
								} else {
									return 'color-gray-1';
								}
							}}
							columns={columnsTable.filter(
								(col) =>
									col.dataIndex !== 'uuid' &&
									col.dataIndex !== 'specification_id' &&
									col.dataIndex !== 'material_group' &&
									col.dataIndex !== 'group_id' &&
									col.dataIndex !== 'state' &&
									col.dataIndex !== 'type' &&
									col.dataIndex !== 'characteristic_id'
							)}
							dataSource={dataListMasterSpec}
						/>
					</TabPane>
					<TabPane tab='Flute General' key='2'>
						<Row
							justify='space-between'
							style={{
								marginBottom: 12,
							}}>
							<Button
								onClick={() =>
									setIsModalActionVisible({
										...isModalActionVisible,
										typeAction: 'ADD FLUTE GENERAL',
										isShowModalAction: true,
									})
								}
								style={{
									backgroundColor: ColorPrimaryEnum.greenTosca,
									border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
								}}
								type='primary'
								icon={<FileAddOutlined />}>
								Add
							</Button>
							<Input.Search
								onSearch={onSearchFluteGeneral}
								allowClear
								style={{ width: '40%' }}
							/>
						</Row>
						<Table
							loading={isLoading}
							bordered
							rowClassName={(record, index) => {
								if (index % 2 === 1) {
									return 'color-gray-2';
								} else {
									return 'color-gray-1';
								}
							}}
							columns={columnsTableFluteGeneral.filter(
								(col) =>
									col.dataIndex !== 'uuid' &&
									col.dataIndex !== 'created_at' &&
									col.dataIndex !== 'updated_at' &&
									col.dataIndex !== 'state' &&
									col.dataIndex !== 'type' &&
									col.dataIndex !== 'material_group' &&
									col.dataIndex !== 'specification_id'
							)}
							dataSource={dataListFluteGeneral}
						/>
					</TabPane>
					<TabPane tab='Flute by Vendor' key='3'>
						<Row
							justify='space-between'
							style={{
								marginBottom: 12,
							}}>
							<Button
								onClick={() =>
									setIsModalActionVisible({
										...isModalActionVisible,
										typeAction: 'ADD FLUTE BY VENDOR',
										isShowModalAction: true,
									})
								}
								style={{
									backgroundColor: ColorPrimaryEnum.greenTosca,
									border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
								}}
								type='primary'
								icon={<FileAddOutlined />}>
								Add
							</Button>
							<Input.Search
								onSearch={onSearchFluteVendor}
								allowClear
								style={{ width: '40%' }}
							/>
						</Row>
						<Table
							loading={isLoading}
							bordered
							rowClassName={(record, index) => {
								if (index % 2 === 1) {
									return 'color-gray-2';
								} else {
									return 'color-gray-1';
								}
							}}
							columns={columnsTableFluteByVendor.filter(
								(col) =>
									col.dataIndex !== 'uuid' &&
									col.dataIndex !== 'created_at' &&
									col.dataIndex !== 'updated_at' &&
									col.dataIndex !== 'material_group' &&
									col.dataIndex !== 'vendor_id' &&
									col.dataIndex !== 'specification_id'
							)}
							dataSource={dataListFluteVendor}
						/>
					</TabPane>
					<TabPane tab='Layer/Sekat' key='4'>
						<Row
							justify='space-between'
							style={{
								marginBottom: 12,
							}}>
							<Button
								onClick={() =>
									setIsModalActionVisible({
										...isModalActionVisible,
										typeAction: 'ADD LAYER SEKAT',
										isShowModalAction: true,
									})
								}
								style={{
									backgroundColor: ColorPrimaryEnum.greenTosca,
									border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
								}}
								type='primary'
								icon={<FileAddOutlined />}>
								Add
							</Button>
							<Input.Search
								onSearch={onSearchLayerSekat}
								allowClear
								style={{ width: '40%' }}
							/>
						</Row>
						<Table
							loading={isLoading}
							bordered
							rowClassName={(record, index) => {
								if (index % 2 === 1) {
									return 'color-gray-2';
								} else {
									return 'color-gray-1';
								}
							}}
							columns={columnsTableLayerSekat.filter(
								(col) =>
									col.dataIndex !== 'uuid' &&
									col.dataIndex !== 'created_at' &&
									col.dataIndex !== 'updated_at' &&
									col.dataIndex !== 'material_group' &&
									col.dataIndex !== 'vendor_id' &&
									col.dataIndex !== 'constanta_reference' &&
									col.dataIndex !== 'specification_id'
							)}
							dataSource={dataListLayerSekat}
						/>
					</TabPane>
				</Tabs>
			</div>

			<Modal
				title={typeAction}
				forceRender={false}
				visible={isShowModalAction}
				width={
					typeAction === 'ADD SPEC' ||
					typeAction === 'EDIT SPEC' ||
					typeAction === 'EDIT FLUTE GENERAL' ||
					typeAction === 'EDIT FLUTE BY VENDOR' ||
					typeAction === 'ADD FLUTE BY VENDOR' ||
					typeAction === 'ADD LAYER SEKAT' ||
					typeAction === 'EDIT LAYER SEKAT' ||
					typeAction === 'ADD FLUTE GENERAL'
						? 1000
						: undefined
				}
				afterClose={() => form.resetFields()}
				onOk={() => {
					if (typeAction === 'DELETE SPEC') {
						if (isLoading) {
							return;
						} else {
							onDelete(dataRow.id);
						}
					}
					if (typeAction === 'DELETE FLUTE GENERAL') {
						if (isLoading) {
							return;
						} else {
							onDeleteFluteGeneral(dataRow.id);
						}
					}
					if (typeAction === 'DELETE FLUTE BY VENDOR') {
						if (isLoading) {
							return;
						} else {
							onDeleteFluteByVendor(dataRow.id);
						}
					}
					if (typeAction === 'DELETE LAYER SEKAT') {
						if (isLoading) {
							return;
						} else {
							onDeleteLayerSekat(dataRow.id);
						}
					} else {
						form.resetFields();
						setIsModalActionVisible({
							...isModalActionVisible,
							isShowModalAction: false,
						});
					}
				}}
				onCancel={() => {
					form.resetFields();
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
				}}>
				{typeAction === 'VIEW' ? (
					dataRow &&
					Object.keys(dataRow).map((data, index) => {
						if (data === 'key' || data === 'guid') {
							return null;
						}
						return (
							<p key={index} style={{ fontWeight: 'bold', color: '#595959' }}>
								{data.replace('_', ' ')} :{' '}
								<span style={{ fontWeight: 'normal' }}>{dataRow[data]}</span>
							</p>
						);
					})
				) : typeAction === 'EDIT SPEC' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='edit'
						onFinish={onEdit}
						fields={[
							{
								name: ['characteristic_id'],
								value: dataRow.characteristic_id,
							},
							{
								name: ['group_id'],
								value: dataRow.group_id,
							},
							{
								name: ['characteristic_description'],
								value: dataRow.characteristic_description,
							},
							{
								name: ['range'],
								value: dataRow.range,
							},
						]}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='characteristic_id'
									label='Characteristic'
									rules={[
										{
											required: true,
											message: 'Please input your characteristic!',
										},
									]}
									hasFeedback>
									<Select
										loading={isLoading}
										placeholder='select your characteristic'>
										{dataCharacter &&
											dataCharacter.map((data, index) => {
												const { id, char_code_variable } = data;
												return (
													<Option key={index} value={id}>
														{char_code_variable}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='group_id'
									label='Group'
									help='1 = Warna, 2 = Bahan, 3 = Flute'
									rules={[
										{
											required: true,
											message: 'Please input group!',
										},
									]}
									hasFeedback>
									<Select loading={isLoading} placeholder='select your group'>
										{dataGroup &&
											Object.keys(dataGroup).map((data, index) => {
												return (
													<Option key={index} value={data}>
														{dataGroup[data]}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='characteristic_description'
									label='Characteristic desc.'
									rules={[
										{
											required: true,
											message: 'Please input your characteristic desc.!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='range'
									label='Range'
									rules={[
										{
											required: true,
											message: 'Please input range!',
										},
									]}
									hasFeedback>
									<Radio.Group>
										<Radio value={'Yes'}>Yes</Radio>
										<Radio value={'No'}>No</Radio>
									</Radio.Group>
								</Form.Item>
							</Col>
						</Row>

						<Form.Item hidden name='guid' label='guid'>
							<Input />
						</Form.Item>
						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Save
								</Button>
							</Form.Item>
						</div>
					</Form>
				) : typeAction === 'ADD SPEC' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='add'
						onFinish={onFinish}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='characteristic_id'
									label='Characteristic'
									rules={[
										{
											required: true,
											message: 'Please input your characteristic!',
										},
									]}
									hasFeedback>
									<Select
										loading={isLoading}
										placeholder='select your characteristic'>
										{dataCharacter &&
											dataCharacter.map((data, index) => {
												const { id, char_code_variable } = data;
												return (
													<Option key={index} value={id}>
														{char_code_variable}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='group_id'
									label='Group'
									help='1 = Warna, 2 = Bahan, 3 = Flute'
									rules={[
										{
											required: true,
											message: 'Please input group!',
										},
									]}
									hasFeedback>
									<Select loading={isLoading} placeholder='select your group'>
										{dataGroup &&
											Object.keys(dataGroup).map((data, index) => {
												return (
													<Option key={index} value={data}>
														{dataGroup[data]}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='characteristic_description'
									label='Characteristic desc.'
									rules={[
										{
											required: true,
											message: 'Please input your characteristic desc.!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='range'
									label='Range'
									rules={[
										{
											required: true,
											message: 'Please input range!',
										},
									]}
									hasFeedback>
									<Radio.Group>
										<Radio value={'Yes'}>Yes</Radio>
										<Radio value={'No'}>No</Radio>
									</Radio.Group>
								</Form.Item>
							</Col>
						</Row>

						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Create
								</Button>
							</Form.Item>
						</div>
					</Form>
				) : typeAction === 'ADD FLUTE GENERAL' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='add'
						onFinish={onFinishFluteGeneral}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='flute'
									label='flute'
									rules={[
										{
											required: true,
											message: 'Please input your flute!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='p_tol'
									label='P Tol'
									rules={[
										{
											required: true,
											message: 'Please input p tol!',
										},
									]}
									hasFeedback>
									<Input type='number' />
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='l_tol'
									label='L Tol'
									rules={[
										{
											required: true,
											message: 'Please input your l tol!',
										},
									]}
									hasFeedback>
									<Input type='number' />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='take_up'
									label='Take Up'
									rules={[
										{
											required: true,
											message: 'Please input take up!',
										},
									]}
									hasFeedback>
									<Input type='number' />
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='kuping'
									label='Kuping'
									rules={[
										{
											required: true,
											message: 'Please input your kuping!',
										},
									]}
									hasFeedback>
									<Input type='number' />
								</Form.Item>
							</Col>
						</Row>

						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Create
								</Button>
							</Form.Item>
						</div>
					</Form>
				) : typeAction === 'ADD FLUTE BY VENDOR' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='add'
						onFinish={onFinishFluteByVendor}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='vendor_id'
									label='Vendor'
									rules={[
										{
											required: true,
											message: 'Please input your vendor!',
										},
									]}
									hasFeedback>
									<Select loading={isLoading} placeholder='select your vendor'>
										{dataListVendor &&
											dataListVendor.map((data, index) => {
												const { name, vendor_code, id } = data;
												return (
													<Option key={index} value={id}>
														{`${vendor_code} - ${name}`}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Create
								</Button>
							</Form.Item>
						</div>
					</Form>
				) : typeAction === 'ADD LAYER SEKAT' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='add'
						onFinish={onFinishLayerSekat}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='vendor_id'
									label='Vendor'
									rules={[
										{
											required: true,
											message: 'Please input your vendor!',
										},
									]}
									hasFeedback>
									<Select loading={isLoading} placeholder='select your vendor'>
										{dataListVendor &&
											dataListVendor.map((data, index) => {
												const { name, vendor_code, id } = data;
												return (
													<Option key={index} value={id}>
														{`${vendor_code} - ${name}`}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='type'
									label='Type'
									rules={[
										{
											required: true,
											message: 'Please input your type!',
										},
									]}
									hasFeedback>
									<Select placeholder='select your type'>
										{dataTypeLayerSekat &&
											Object.keys(dataTypeLayerSekat).map((data, index) => {
												return (
													<Option key={index} value={data}>
														{data}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Create
								</Button>
							</Form.Item>
						</div>
					</Form>
				) : typeAction === 'EDIT LAYER SEKAT' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='edit'
						fields={[
							{
								name: ['vendor_id'],
								value: dataRow.vendor_id,
							},
							{
								name: ['type'],
								value: dataRow.type,
							},
						]}
						onFinish={onEditLayerSekat}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='vendor_id'
									label='Vendor'
									rules={[
										{
											required: true,
											message: 'Please input your vendor!',
										},
									]}
									hasFeedback>
									<Select loading={isLoading} placeholder='select your vendor'>
										{dataListVendor &&
											dataListVendor.map((data, index) => {
												const { name, vendor_code, id } = data;
												return (
													<Option key={index} value={id}>
														{`${vendor_code} - ${name}`}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='type'
									label='Type'
									rules={[
										{
											required: true,
											message: 'Please input your type!',
										},
									]}
									hasFeedback>
									<Select placeholder='select your type'>
										{dataTypeLayerSekat &&
											Object.keys(dataTypeLayerSekat).map((data, index) => {
												return (
													<Option key={index} value={data}>
														{data}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Save
								</Button>
							</Form.Item>
						</div>
					</Form>
				) : typeAction === 'EDIT FLUTE BY VENDOR' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='edit'
						fields={[
							{
								name: ['vendor_id'],
								value: dataRow.vendor_id,
							},
						]}
						onFinish={onEditFluteByVendor}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='vendor_id'
									label='Vendor'
									rules={[
										{
											required: true,
											message: 'Please input your vendor!',
										},
									]}
									hasFeedback>
									<Select loading={isLoading} placeholder='select your vendor'>
										{dataListVendor &&
											dataListVendor.map((data, index) => {
												const { name, vendor_code, id } = data;
												return (
													<Option key={index} value={id}>
														{`${vendor_code} - ${name}`}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Save
								</Button>
							</Form.Item>
						</div>
					</Form>
				) : typeAction === 'EDIT FLUTE GENERAL' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='edit'
						fields={[
							{
								name: ['flute'],
								value: dataRow.flute,
							},
							{
								name: ['p_tol'],
								value: dataRow.p_tol,
							},
							{
								name: ['l_tol'],
								value: dataRow.l_tol,
							},
							{
								name: ['take_up'],
								value: dataRow.take_up,
							},
							{
								name: ['kuping'],
								value: dataRow.kuping,
							},
						]}
						onFinish={onEditFluteGeneral}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='flute'
									label='flute'
									rules={[
										{
											required: true,
											message: 'Please input your flute!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='p_tol'
									label='P Tol'
									rules={[
										{
											required: true,
											message: 'Please input p tol!',
										},
									]}
									hasFeedback>
									<Input type='number' />
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='l_tol'
									label='L Tol'
									rules={[
										{
											required: true,
											message: 'Please input your l tol!',
										},
									]}
									hasFeedback>
									<Input type='number' />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='take_up'
									label='Take Up'
									rules={[
										{
											required: true,
											message: 'Please input take up!',
										},
									]}
									hasFeedback>
									<Input type='number' />
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='kuping'
									label='Kuping'
									rules={[
										{
											required: true,
											message: 'Please input your kuping!',
										},
									]}
									hasFeedback>
									<Input type='number' />
								</Form.Item>
							</Col>
						</Row>

						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Save
								</Button>
							</Form.Item>
						</div>
					</Form>
				) : typeAction === 'DELETE SPEC' ? (
					<p>
						Apakah anda ingin menghapus daata <b>{dataRow?.characteristic}</b>{' '}
						ini ?
					</p>
				) : typeAction === 'DELETE FLUTE GENERAL' ? (
					<p>
						Apakah anda ingin menghapus daata <b>{dataRow?.flute}</b> ini ?
					</p>
				) : typeAction === 'DELETE FLUTE BY VENDOR' ? (
					<p>
						Apakah anda ingin menghapus daata{' '}
						<b>{dataRow?.material_group_description}</b> ini ?
					</p>
				) : (
					<p>
						Apakah anda ingin menghapus daata{' '}
						<b>{dataRow?.material_group_description}</b> ini ?
					</p>
				)}
			</Modal>
			{contextHolder}
		</Content>
	);
};

export default MasterSpesificationCartonBox;
