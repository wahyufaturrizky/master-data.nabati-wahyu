import {
	FileAddOutlined,
	InboxOutlined,
	UploadOutlined,
} from '@ant-design/icons';
import { Button, Form, Layout, Radio, Select, Typography, Upload } from 'antd';
import React, { useState } from 'react';
import { ColorBaseEnum, ColorPrimaryEnum } from '../styles/Colors';

const { Content } = Layout;
const { Option } = Select;
const { Title } = Typography;

const UploadRFQ = (props) => {
	const [form] = Form.useForm();
	const [isSpesification, setIsSpesification] = useState(false);
	const normFile = (e: any) => {
		console.log('Upload event:', e);
		if (Array.isArray(e)) {
			return e;
		}
		return e && e.fileList;
	};

	return (
		<Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
			<Title level={4}>RFQ Upload</Title>
			<div className='site-layout-background' style={{ padding: 24 }}>
				<Form scrollToFirstError form={form}>
					<Form.Item
						rules={[
							{
								required: true,
								message: 'Please input type!',
							},
						]}
						name='type'
						label='Type:'>
						<Radio.Group
							onChange={(e) =>
								setIsSpesification(e.target.value === 'specification' ?? false)
							}
							value='sku'>
							<Radio value='sku'>SKU</Radio>
							<Radio value='specification'>Specification</Radio>
						</Radio.Group>
					</Form.Item>

					{isSpesification && (
						<Form.Item
							name='materialGroup'
							label='Material Group'
							rules={[
								{
									required: true,
									message: 'Please input material group!',
								},
							]}
							hasFeedback>
							<Select
								allowClear
								showSearch
								style={{ width: 200 }}
								placeholder='Select'
								optionFilterProp='children'
								onChange={(e) => console.log(`onChange materialGroup ${e}`)}
								onFocus={(e) => console.log(`onFocus materialGroup ${e}`)}
								onBlur={(e) => console.log(`onBlur materialGroup ${e}`)}
								onSearch={(e) => console.log(`onSearch materialGroup ${e}`)}
								filterOption={(input, option) =>
									option.children.toLowerCase().indexOf(input.toLowerCase()) >=
									0
								}>
								<Option value='jack'>Jack</Option>
								<Option value='lucy'>Lucy</Option>
								<Option value='tom'>Tom</Option>
							</Select>
						</Form.Item>
					)}

					<Form.Item>
						<Form.Item
							name='uploadFileRFQ'
							valuePropName='fileList'
							getValueFromEvent={normFile}
							noStyle>
							<Upload.Dragger name='files' action='/upload.do'>
								<p className='ant-upload-drag-icon'>
									<InboxOutlined />
								</p>
								<p className='ant-upload-text'>
									Click or drag file to this area to upload
								</p>
								<p className='ant-upload-hint'>
									Support for a single or bulk upload.
								</p>
							</Upload.Dragger>
						</Form.Item>
					</Form.Item>

					<Form.Item>
						<Button
							htmlType='submit'
							block
							style={{
								backgroundColor: ColorBaseEnum.white,
								color: ColorPrimaryEnum.redVelvet,
								border: `1px solid ${ColorPrimaryEnum.redVelvet}`,
							}}
							type='primary'
							icon={<UploadOutlined />}>
							Upload File
						</Button>
					</Form.Item>
				</Form>

				<Button
					block
					style={{
						backgroundColor: ColorBaseEnum.white,
						color: ColorPrimaryEnum.greenTosca,
						border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
					}}
					type='primary'
					icon={<FileAddOutlined />}>
					Download Template
				</Button>
			</div>
		</Content>
	);
};

export default UploadRFQ;
