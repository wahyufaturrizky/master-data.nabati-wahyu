import { DownloadOutlined, EyeOutlined } from '@ant-design/icons';
import {
	Button,
	DatePicker,
	Layout,
	Radio,
	Select,
	Typography,
	Space,
	Row,
	Table,
} from 'antd';
import React, { useState } from 'react';
import { ColorBaseEnum, ColorPrimaryEnum } from '../styles/Colors';
import { MarginEnum } from '../styles/Spacer';

const { Content } = Layout;
const { Option } = Select;
const { Title, Text } = Typography;

const ReportVendorPerformance = (props) => {
	const [isLoading, setIsLoading] = useState(false);

	const dataTable = [
		{
			key: 1,
			company: 'KSNI',
			vendor: '200012',
			vendorDesc: 'Century MMS PT',
			materialGroup: '01',
			materialGroupDesc: 'Roll',
			achivement: '100%',
			validFrom: '01-04-2021',
			validTo: '01-08-2021',
			createOn: '01-08-2021',
			createBy: '01-08-2021',
		},
	];

	const columnsTable = [
		{
			title: 'No',
			dataIndex: 'key',
			key: 'key',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Company',
			dataIndex: 'company',
			key: 'company',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Vendor',
			dataIndex: 'vendor',
			key: 'vendor',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Vendor Description',
			dataIndex: 'vendorDesc',
			key: 'vendorDesc',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Group',
			dataIndex: 'materialGroup',
			key: 'materialGroup',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Group Desc.',
			dataIndex: 'materialGroupDesc',
			key: 'materialGroupDesc',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Achivement',
			dataIndex: 'achivement',
			key: 'achivement',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Valid From',
			dataIndex: 'validFrom',
			key: 'validFrom',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Valid To',
			dataIndex: 'validTo',
			key: 'validTo',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Create On',
			dataIndex: 'createOn',
			key: 'createOn',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Create By',
			dataIndex: 'createBy',
			key: 'createBy',
			render: (text) => <p>{text}</p>,
		},
	];

	return (
		<Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
			<Title level={4}>Vendor Performance Report</Title>
			<div className='site-layout-background' style={{ padding: 24 }}>
				<div style={{ marginBottom: MarginEnum['2x'] }}>
					<Radio.Group value='sku'>
						<Radio value='sku'>SKU</Radio>
						<Radio value='specification'>Specification</Radio>
					</Radio.Group>
				</div>

				<Row
					style={{
						marginBottom: MarginEnum['2x'],
					}}>
					<Space>
						<Space direction='vertical'>
							<Text>Vendor</Text>
							<Select
								allowClear
								showSearch
								style={{ width: 200 }}
								placeholder='Select'
								optionFilterProp='children'
								onChange={(e) => console.log(`onChange vendor ${e}`)}
								onFocus={(e) => console.log(`onFocus vendor ${e}`)}
								onBlur={(e) => console.log(`onBlur vendor ${e}`)}
								onSearch={(e) => console.log(`onSearch vendor ${e}`)}
								filterOption={(input, option) =>
									option.children.toLowerCase().indexOf(input.toLowerCase()) >=
									0
								}>
								<Option value='jack'>Jack</Option>
								<Option value='lucy'>Lucy</Option>
								<Option value='tom'>Tom</Option>
							</Select>
						</Space>
						<Space direction='vertical'>
							<Text>Valid On</Text>
							<DatePicker />
						</Space>
						<Space direction='vertical'>
							<Text>Company</Text>
							<Select
								allowClear
								showSearch
								style={{ width: 200 }}
								placeholder='Select'
								optionFilterProp='children'
								onChange={(e) => console.log(`onChange company ${e}`)}
								onFocus={(e) => console.log(`onFocus company ${e}`)}
								onBlur={(e) => console.log(`onBlur company ${e}`)}
								onSearch={(e) => console.log(`onSearch company ${e}`)}
								filterOption={(input, option) =>
									option.children.toLowerCase().indexOf(input.toLowerCase()) >=
									0
								}>
								<Option value='jack'>Jack</Option>
								<Option value='lucy'>Lucy</Option>
								<Option value='tom'>Tom</Option>
							</Select>
						</Space>
						<Space direction='vertical'>
							<Text style={{ color: ColorBaseEnum.white }}>text</Text>
							<Button
								style={{
									backgroundColor: ColorPrimaryEnum.redVelvet,
									border: `1px solid ${ColorPrimaryEnum.redVelvet}`,
								}}
								type='primary'
								icon={<EyeOutlined />}>
								View
							</Button>
						</Space>
						<Space direction='vertical'>
							<Text style={{ color: ColorBaseEnum.white }}>text</Text>
							<Button
								style={{
									backgroundColor: ColorPrimaryEnum.greenTosca,
									border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
								}}
								type='primary'
								icon={<DownloadOutlined />}>
								Download
							</Button>
						</Space>
					</Space>
				</Row>
				<Table
					loading={isLoading}
					bordered
					columns={columnsTable}
					dataSource={dataTable}
					scroll={{ x: 1500 }}
				/>
			</div>
		</Content>
	);
};

export default ReportVendorPerformance;
