import {
	CloudDownloadOutlined,
	CloudUploadOutlined,
	DeleteOutlined,
	EditOutlined,
	EyeOutlined,
	FileAddOutlined,
	InboxOutlined,
	UploadOutlined,
} from '@ant-design/icons';
import {
	Button,
	Col,
	Form,
	Input,
	Layout,
	Modal,
	Row,
	Select,
	Space,
	Table,
	Typography,
	Upload,
} from 'antd';
import React, { useState } from 'react';
import { ColorBaseEnum, ColorPrimaryEnum } from '../styles/Colors';
import { MarginEnum } from '../styles/Spacer';

const { Content } = Layout;
const { Option } = Select;
const { Title } = Typography;

const formItemLayout = {
	labelCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 8,
		},
	},
	wrapperCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 16,
		},
	},
};

const MasterDesign = (props) => {
	const prefixSelector = (
		<Form.Item name='prefix' noStyle>
			<Select
				style={{
					width: 70,
				}}>
				<Option value='62'>+62</Option>
				<Option value='87'>+87</Option>
			</Select>
		</Form.Item>
	);

	const [modal, contextHolder] = Modal.useModal();
	const [form] = Form.useForm();
	const [isModalActionVisible, setIsModalActionVisible] = useState({
		dataRow: null,
		typeAction: '',
		isShowModalAction: false,
	});
	const [isLoading, setIsLoading] = useState(false);
	const { isShowModalAction, typeAction, dataRow } = isModalActionVisible;

	const normFile = (e: any) => {
		console.log('Upload event:', e);
		if (Array.isArray(e)) {
			return e;
		}
		return e && e.fileList;
	};

	const config = {
		title: 'Success Message!',
		content: <p>Success create supplier!</p>,
		onOk: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			window.location.reload();
		},
		onCancel: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
		},
	};

	const updateSupllier = {
		title: 'Success Message!',
		content: <p>Success update supplier!</p>,
		onOk: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			window.location.reload();
		},
		onCancel: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
		},
	};

	const onFinish = async (values) => {
		setIsLoading(true);
		modal.confirm('add');
		setIsLoading(false);
	};

	const onEditSupplier = async (values) => {
		setIsLoading(true);
		modal.confirm('udpate');
		setIsLoading(false);
	};

	const onDeleteSupplier = async (values) => {
		window.alert('Success delete supplier');
		setIsModalActionVisible({
			...isModalActionVisible,
			isShowModalAction: false,
		});
		window.location.reload();
	};

	const dataTable = [
		{
			key: 1,
			vendor: '2000014',
			vendorDesc: 'Tunas Alfin, PT',
			material: '2011202012',
			materialDesc: 'Roll Nabati RCE 20gr D4',
			oldMaterial: '2011202009',
			oldMaterialDesc: 'Roll Nabati RCE 20gr D3',
			activeDesign: 'No',
			createOn: '14-08-2021',
			createBy: 'Sarah',
			changeOn: '17-09-2021',
			changeBy: 'Sarah',
			activePR: 'Sarah',
			status: 'Develop',
		},
	];

	const columnsTable = [
		{
			title: 'No',
			dataIndex: 'key',
			key: 'key',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Vendor',
			dataIndex: 'vendor',
			key: 'vendor',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Vendor Description',
			dataIndex: 'vendorDesc',
			key: 'vendorDesc',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material',
			dataIndex: 'material',
			key: 'material',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Material Description',
			dataIndex: 'materialDesc',
			key: 'materialDesc',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Old Material',
			dataIndex: 'oldMaterial',
			key: 'oldMaterial',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Old Material Description',
			dataIndex: 'OldMaterialDesc',
			key: 'OldMaterialDesc',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Active Design',
			dataIndex: 'activeDesign',
			key: 'activeDesign',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Create',
			dataIndex: 'create',
			key: 'create',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Create By',
			dataIndex: 'createBy',
			key: 'createBy',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Change On',
			dataIndex: 'changeOn',
			key: 'changeOn',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Change By',
			dataIndex: 'changeBy',
			key: 'changeBy',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Active PR',
			dataIndex: 'activePR',
			key: 'activePR',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Status',
			dataIndex: 'status',
			key: 'status',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Action',
			key: 'action',
			render: (text, record) => {
				return (
					<Space size='middle'>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'VIEW',
								})
							}
							type='primary'
							icon={<EyeOutlined />}>
							View
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'EDIT',
								})
							}
							type='default'
							icon={<EditOutlined />}>
							Edit
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									...isModalActionVisible,
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'DELETE',
								})
							}
							type='danger'
							icon={<DeleteOutlined />}>
							Delete
						</Button>
					</Space>
				);
			},
		},
	];

	return (
		<Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
			<Title level={4}>MD Design</Title>
			<div className='site-layout-background' style={{ padding: 24 }}>
				<Row
					justify='end'
					style={{
						marginBottom: 12,
					}}>
					<Space>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									...isModalActionVisible,
									typeAction: 'ADD',
									isShowModalAction: true,
								})
							}
							style={{
								backgroundColor: ColorPrimaryEnum.greenTosca,
								border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
							}}
							type='primary'
							icon={<FileAddOutlined />}>
							Add
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									...isModalActionVisible,
									typeAction: 'UPLOAD',
									isShowModalAction: true,
								})
							}
							style={{
								backgroundColor: ColorPrimaryEnum.redVelvet,
								border: `1px solid ${ColorPrimaryEnum.redVelvet}`,
							}}
							type='primary'
							icon={<CloudUploadOutlined />}>
							Upload
						</Button>
						<Button
							style={{
								backgroundColor: ColorPrimaryEnum.redVelvet,
								border: `1px solid ${ColorPrimaryEnum.redVelvet}`,
							}}
							type='primary'
							icon={<CloudDownloadOutlined />}>
							Download
						</Button>
					</Space>
				</Row>
				<Table
					loading={isLoading}
					bordered
					columns={columnsTable}
					dataSource={dataTable}
					scroll={{ x: 1900 }}
				/>
			</div>

			<Modal
				title={typeAction}
				forceRender={false}
				visible={isShowModalAction}
				width={typeAction === 'ADD' || typeAction === 'EDIT' ? 1000 : undefined}
				afterClose={() => form.resetFields()}
				onOk={() => {
					if (typeAction === 'DELETE') {
						onDeleteSupplier(dataRow.guid);
					} else {
						form.resetFields();
						setIsModalActionVisible({
							...isModalActionVisible,
							isShowModalAction: false,
						});
					}
				}}
				onCancel={() => {
					form.resetFields();
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
				}}>
				{typeAction === 'VIEW' ? (
					dataRow &&
					Object.keys(dataRow).map((data, index) => {
						if (
							data === 'key' ||
							data === 'guid' ||
							data === 'updated_at' ||
							data === 'created_at'
						) {
							return null;
						}
						return (
							<p key={index} style={{ fontWeight: 'bold', color: '#595959' }}>
								{data.replace('_', ' ')} :{' '}
								<span style={{ fontWeight: 'normal' }}>{dataRow[data]}</span>
							</p>
						);
					})
				) : typeAction === 'EDIT' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='addSupplier'
						onFinish={onEditSupplier}
						fields={[
							{
								name: ['vendor'],
								value: dataRow.vendor,
							},
							{
								name: ['activeDesign'],
								value: dataRow.activeDesign,
							},
							{
								name: ['material'],
								value: dataRow.material,
							},
							{
								name: ['activePR'],
								value: dataRow.activePR,
							},
							{
								name: ['oldMaterial'],
								value: dataRow.oldMaterial,
							},
							{
								name: ['status'],
								value: dataRow.status,
							},
						]}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='vendor'
									label='Vendor'
									rules={[
										{
											required: true,
											message: 'Please input vendor!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange vendor ${e}`)}
										onFocus={(e) => console.log(`onFocus vendor ${e}`)}
										onBlur={(e) => console.log(`onBlur vendor ${e}`)}
										onSearch={(e) => console.log(`onSearch vendor ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='activeDesign'
									label='Active Design'
									rules={[
										{
											required: true,
											message: 'Please input your active design!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange active design ${e}`)}
										onFocus={(e) => console.log(`onFocus active design ${e}`)}
										onBlur={(e) => console.log(`onBlur active design ${e}`)}
										onSearch={(e) => console.log(`onSearch active design ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='material'
									label='Material'
									rules={[
										{
											required: true,
											message: 'Please input material!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange material ${e}`)}
										onFocus={(e) => console.log(`onFocus material ${e}`)}
										onBlur={(e) => console.log(`onBlur material ${e}`)}
										onSearch={(e) => console.log(`onSearch material ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='activePR'
									label='Active PR'
									rules={[
										{
											required: true,
											message: 'Please input your active PR!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) =>
											console.log(`onChange active activePR ${e}`)
										}
										onFocus={(e) => console.log(`onFocus active activePR ${e}`)}
										onBlur={(e) => console.log(`onBlur active activePR ${e}`)}
										onSearch={(e) =>
											console.log(`onSearch active activePR ${e}`)
										}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='oldMaterial'
									label='Old Material'
									rules={[
										{
											required: true,
											message: 'Please input old material!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange Old Material ${e}`)}
										onFocus={(e) => console.log(`onFocus Old Material ${e}`)}
										onBlur={(e) => console.log(`onBlur Old Material ${e}`)}
										onSearch={(e) => console.log(`onSearch Old Material ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='status'
									label='Status'
									rules={[
										{
											required: true,
											message: 'Please input your status!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange active status ${e}`)}
										onFocus={(e) => console.log(`onFocus active status ${e}`)}
										onBlur={(e) => console.log(`onBlur active status ${e}`)}
										onSearch={(e) => console.log(`onSearch active status ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<Form.Item hidden name='guid' label='guid'>
							<Input />
						</Form.Item>
						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Save
								</Button>
							</Form.Item>
							{contextHolder}
						</div>
					</Form>
				) : typeAction === 'ADD' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='addSupplier'
						onFinish={onFinish}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='vendor'
									label='Vendor'
									rules={[
										{
											required: true,
											message: 'Please input vendor!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange vendor ${e}`)}
										onFocus={(e) => console.log(`onFocus vendor ${e}`)}
										onBlur={(e) => console.log(`onBlur vendor ${e}`)}
										onSearch={(e) => console.log(`onSearch vendor ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='activeDesign'
									label='Active Design'
									rules={[
										{
											required: true,
											message: 'Please input your active design!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange active design ${e}`)}
										onFocus={(e) => console.log(`onFocus active design ${e}`)}
										onBlur={(e) => console.log(`onBlur active design ${e}`)}
										onSearch={(e) => console.log(`onSearch active design ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='material'
									label='Material'
									rules={[
										{
											required: true,
											message: 'Please input material!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange material ${e}`)}
										onFocus={(e) => console.log(`onFocus material ${e}`)}
										onBlur={(e) => console.log(`onBlur material ${e}`)}
										onSearch={(e) => console.log(`onSearch material ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='activePR'
									label='Active PR'
									rules={[
										{
											required: true,
											message: 'Please input your active PR!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) =>
											console.log(`onChange active activePR ${e}`)
										}
										onFocus={(e) => console.log(`onFocus active activePR ${e}`)}
										onBlur={(e) => console.log(`onBlur active activePR ${e}`)}
										onSearch={(e) =>
											console.log(`onSearch active activePR ${e}`)
										}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='oldMaterial'
									label='Old Material'
									rules={[
										{
											required: true,
											message: 'Please input old material!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange Old Material ${e}`)}
										onFocus={(e) => console.log(`onFocus Old Material ${e}`)}
										onBlur={(e) => console.log(`onBlur Old Material ${e}`)}
										onSearch={(e) => console.log(`onSearch Old Material ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='status'
									label='Status'
									rules={[
										{
											required: true,
											message: 'Please input your status!',
										},
									]}
									hasFeedback>
									<Select
										allowClear
										showSearch
										style={{ width: 200 }}
										placeholder='Select'
										optionFilterProp='children'
										onChange={(e) => console.log(`onChange active status ${e}`)}
										onFocus={(e) => console.log(`onFocus active status ${e}`)}
										onBlur={(e) => console.log(`onBlur active status ${e}`)}
										onSearch={(e) => console.log(`onSearch active status ${e}`)}
										filterOption={(input, option) =>
											option.children
												.toLowerCase()
												.indexOf(input.toLowerCase()) >= 0
										}>
										<Option value='jack'>Jack</Option>
										<Option value='lucy'>Lucy</Option>
										<Option value='tom'>Tom</Option>
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Create
								</Button>
							</Form.Item>
							{contextHolder}
						</div>
					</Form>
				) : typeAction === 'UPLOAD' ? (
					<>
						<Form.Item>
							<Form.Item
								name='uploadFileMdDesign'
								valuePropName='fileList'
								getValueFromEvent={normFile}
								noStyle>
								<Upload.Dragger name='files' action='/upload.do'>
									<p className='ant-upload-drag-icon'>
										<InboxOutlined />
									</p>
									<p className='ant-upload-text'>
										Click or drag file to this area to upload
									</p>
									<p className='ant-upload-hint'>
										Support for a single or bulk upload.
									</p>
								</Upload.Dragger>
							</Form.Item>
						</Form.Item>

						<Button
							block
							style={{
								backgroundColor: ColorBaseEnum.white,
								color: ColorPrimaryEnum.redVelvet,
								border: `1px solid ${ColorPrimaryEnum.redVelvet}`,
								marginBottom: MarginEnum['2x'],
							}}
							type='primary'
							icon={<UploadOutlined />}>
							Upload File
						</Button>
						<Button
							block
							style={{
								backgroundColor: ColorBaseEnum.white,
								color: ColorPrimaryEnum.greenTosca,
								border: `1px solid ${ColorPrimaryEnum.greenTosca}`,
							}}
							type='primary'
							icon={<FileAddOutlined />}>
							Download Template
						</Button>
					</>
				) : (
					<p>Apakah anda ingin menghapus data ini ?</p>
				)}
			</Modal>
		</Content>
	);
};

export default MasterDesign;
